#pragma once

#define ULONG unsigned long
#define UINT unsigned int

#define PRECISION_DEC 20 //Number of decimals that is good enough for correctness using mpf_class
#define PRECISION_BITS 80 //Number of bits that is good enough for correctness using mpf_class 
						  //(basically, it may be automatially rounded up to the nearest power of 2)
#define COMPARISON_EPSILON 3.7252903e-9 //Precision required for correct comparisons (derived from numerical analysis in antoher paper)
#define COMPARISON_EPSILON_KEY 1e-15 //Precision of the key generation procedure, sufficient for correct comparisons and encryption

#include <iostream>
#include <iomanip> 
#include <math.h>
#include <stdarg.h>
#include <string.h>
#include <cassert> 
#include "stdlib.h"
#include <vector>
#include <climits>
#include <sstream>
#include <fstream>
#include <gmpxx.h>
#include <sys/time.h>
#include <iomanip>
#include <gmp.h>
#include <zlib.h>
#include "linalg.h"
#include "alglib/linalg.h"
#include "benchmarks.h"
#include "misc.h"
#include "io.h"
#include "log.h"

#!/usr/bin/env bash

function run {
	PROG_TEST=bin/main
	N=$1 # 100, 1000, 10000
	L=$2 # program name
	WORK=$3 # query workload: Random, Skyserver
	MIN=$4 # domains left bound
	MAX=$5 # domains right bound
	SEL=$6  # selectivity: 1e-2 ... 1e-6
    QNUM=$7    # maximum runtime limit in seconds
	MODE=$8    # plain, enc, ambig = plain or encrypted or ambigous
	RES_DIR=res/$N/$L

	make -s $PROG_TEST
	$PROG_TEST $N $L $WORK $MIN $MAX $SEL $QNUM $MODE > /dev/null #output file may become very large, thus, is not saved during experiments (only used for debugging)

	mkdir -p $RES_DIR
	RES_DESCR=${WORK}_${SEL}_${QNUM}_${MODE}
	mv result_a_b_count.gz ${RES_DIR}/result_a_b_count_${RES_DESCR}.gz
	mv n_touched.gz ${RES_DIR}/n_touched_${RES_DESCR}.gz
	mv treeSearch_t.gz ${RES_DIR}/treeSearch_t_${RES_DESCR}.gz
	mv crack_t.gz ${RES_DIR}/crack_t_${RES_DESCR}.gz
	mv treeInsert_t.gz ${RES_DIR}/treeInsert_t_${RES_DESCR}.gz
	mv total_t.gz ${RES_DIR}/total_t_${RES_DESCR}.gz
	mv total_cumulative_t.gz ${RES_DIR}/total_cumulative_t_${RES_DESCR}.gz
	mv client_t.gz ${RES_DIR}/client_t_${RES_DESCR}.gz
}

run $1 $2 $3 $4 $5 $6 $7 $8

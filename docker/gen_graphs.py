from __future__ import division
import numpy as np
import gzip
import matplotlib
import itertools
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from subprocess import call
import sys

assert(len(sys.argv) == 2)
scale=float(sys.argv[1])

gdr = 'graphs/'
M1 = int(10**6*scale)
M2 = M1
Q = int(50000*scale)

def flip(items, ncol):
    return itertools.chain(*[items[i::ncol] for i in range(ncol)])

def read_floats(f, gz=True):
    ret = []
    if gz:
        fin = gzip.open(f,'r')
    else:
        fin = open(f,'r')
    for line in fin:
        if len(line.rstrip('\n')) > 0:
            ret.append(float(line))
    return np.array(ret)

def get_times_cum(N, l, which):
    assert ('plain' in which or 'enc' in which or 'ambig' in which)
    dr = "res/" + str(N) + "/" + str(l)
    t_cum = read_floats(dr + "/total_cumulative_t_{0}.gz".format(which))
    return t_cum

def get_times(N, l, which):
    assert 'plain' in which or 'enc' in which or 'ambig' in which
    dr = "res/" + str(N) + "/" + str(l)
    t_search = read_floats(dr + "/treeSearch_t_{0}.gz".format(which))
    t_crack = read_floats(dr + "/crack_t_{0}.gz".format(which))
    t_insert = read_floats(dr + "/treeInsert_t_{0}.gz".format(which))
    t_total = read_floats(dr + "/total_t_{0}.gz".format(which))
    assert t_search.shape[0] == t_crack.shape[0] == t_insert.shape[0] == t_total.shape[0]
    return t_crack, t_insert, t_search,  t_total


def figure_6_prepare():
    qnum = Q
    qs = np.arange(1, qnum + 1)
    plain = []
    enc = []
    ambig = []

    Ns = [k*M1 for k in [1,2,4,8,16,32]]
    for N in Ns:
        p = get_times_cum(N, 4, 'Random_1e-2_{0}_plain'.format(qnum))
        e = get_times_cum(N, 4, 'Random_1e-2_{0}_enc'.format(qnum))
        a = get_times_cum(N, 4, 'Random_1e-2_{0}_ambig'.format(qnum))
        plain.append(p)
        enc.append(e)
        ambig.append(a)
        assert(p.shape[0] == e.shape[0] == a.shape[0])
    return plain, enc, ambig
        
def figure_6(plain, enc, ambig):
    qnum = Q
    qs = np.arange(1, qnum + 1)
    names = ['plainCumulative100.pdf', 'EncCumulative100.pdf', 'EncAmbCumulative100.pdf', 'plain_50000.pdf', 'Enc_50000.pdf', 'EncAmb_50000.pdf']
    cols = ['red', 'orange', 'blue', 'lime', 'black', 'violet']
    labels = ["1M","2M", "4M", "8M", "16M", "32M"]
    data = [plain, enc, ambig, plain, enc, ambig]
    nums = [231, 232, 233, 234, 235, 236]
    sizes = [30, 30, 30, qnum, qnum, qnum]
    titles = ["(a) First 30 Queries, Plain", 
              "(b) First 30 Queries, Encrypted", 
              "(c) First 30 Queries, Encrypted with Ambiguity",
              "(d) 50k Queries, Plain", 
              "(e) 50k Queries, Encrypted", 
              "(f) 50k Queries, Encrypted with Ambiguity"
             ]
    
    for i in xrange(6):
        fg = plt.figure()
        plt.title(titles[i])
        for k, (y, col, lbl) in enumerate(zip(data[i], cols, labels)):
            plt.plot(qs[:sizes[i]], y[:sizes[i]], '-', color=col, label=lbl)
            plt.plot(qs[:sizes[i]], enc[k][0]*qs[:sizes[i]], '--', color=col)
            plt.yscale('log')
            plt.xlabel('Query Sequence')
            plt.ylabel('Cumulative Time(secs)')
        ax = fg.axes[0]
        h, l = ax.get_legend_handles_labels()
        lgd = plt.legend(h, l, loc=8, ncol=3, bbox_to_anchor=(0.5, -0.35))
#         plt.show()
        plt.savefig(gdr + names[i], bbox_extra_artists=(lgd,), bbox_inches='tight')


def figure_7_prepare():
    qnum = Q
    qs = np.arange(1, qnum + 1)
    plain = []
    enc = []
    ambig = []
    Ns = [k*M1 for k in [1,2,4,8,16,32]]
    for N in Ns:
        p = get_times_cum(N, 4, 'Random_1e-2_{0}_plain'.format(qnum))
        e = get_times_cum(N, 4, 'Random_1e-2_{0}_enc'.format(qnum))
        a = get_times_cum(N, 4, 'Random_1e-2_{0}_ambig'.format(qnum))
        plain.append(p)
        enc.append(e)
        ambig.append(a)
        assert(p.shape[0] == e.shape[0] == a.shape[0])
    return plain, enc, ambig
        
def figure_7(plain, enc, ambig):
    qnum = Q
    qs = np.arange(1, qnum + 1)
    handles=[]
    cols = ['red', 'orange', 'blue', 'lime', 'black', 'violet']
    labels = ["1M","2M", "4M", "8M", "16M", "32M"]
    # 'Plain', 'Encrypted', 'SecureScan'
    for y, col, lbl in zip(plain, cols, labels):
        plt.plot(qs, y, '-.', color=col)
    for y, col, lbl in zip(enc, cols, labels):
        plt.plot(qs, y, '--', color=col)
    for y, col, lbl in zip(ambig, cols, labels):
        handle, = plt.plot(qs, y, '-', color=col, label=lbl)
        handles.append(handle)
    for y, col, lbl in zip(enc, cols, labels):
        plt.plot(qs, y[0]*qs, ':', color=col)
        
    
    handles.append(mlines.Line2D([], [], color='black', linestyle='-.',
                          markersize=0.5, label='Plain'))
    handles.append(mlines.Line2D([], [], color='black', linestyle='dotted',
                          markersize=0.5, label='SecureScan'))
    handles.append(mlines.Line2D([], [], color='black', linestyle='--',
                          markersize=0.5, label='Encrypted'))
    handles.append(mlines.Line2D([], [], color='black', linestyle='-',
                          markersize=0.5, label='Encrypted with'))
    handles.append(mlines.Line2D([], [], color='black', linestyle='',
                          markersize=0.5, label='Ambiguity'))
        
    plt.yscale('log')
    plt.xlabel('Query Sequence')
    plt.ylabel('Cumulative Time(secs)')
    lgd = plt.legend(handles=handles, loc=8, ncol=4, columnspacing=0.5, bbox_to_anchor=(0.5, -0.45))
    # plt.show()
    plt.savefig(gdr + "comparison.pdf", bbox_extra_artists=(lgd,), bbox_inches='tight')


def figure_8_9_10_prepare():
    qnum = Q
    qs = np.arange(1, qnum + 1)
    plain = []
    enc = []
    ambig = []
    scan = []
    Ns = [k*M1 for k in [1,2,4,8,16,32]]
    for N in Ns:
        plain.append(get_times(N, 4, 'Random_1e-2_{0}_plain'.format(qnum))[:-1])
        enc.append(get_times(N, 4, 'Random_1e-2_{0}_enc'.format(qnum))[:-1])
        ambig.append(get_times(N, 4, 'Random_1e-2_{0}_ambig'.format(qnum))[:-1])
    return plain, enc, ambig
        
def figure_8_9_10(plain, enc, ambig):
    qnum = Q
    qs = np.arange(1, qnum + 1)
    lines=[]
    lines.append(mlines.Line2D([], [], color='red', linestyle='--',
                          markersize=1))
    lines.append(mlines.Line2D([], [], color='violet', linestyle='-',
                          markersize=1))
    lines.append(mlines.Line2D([], [], color='blue', linestyle='-',
                          markersize=1))
    lines.append(mlines.Line2D([], [], color='lime', linestyle='-',
                          markersize=1))
    labels = ['SecureScan', 'Crack', 'Insert', 'Search']
    names = ['plainBreakDown.png', 'EncBreakDown.png', 'EncAmbBreakDown.png']
    cols = ['violet', 'blue', 'lime']
    texts = ['1M', '2M', '4M', '8M', '16M', '32M']
    
    for data, name in zip([plain, enc, ambig], names):
        fig = plt.figure(figsize=(7.58, 7))
        nums = [321, 322, 323, 324, 325, 326]

        mn = np.inf
        mx = -np.inf
        for i in xrange(6):
            for j in xrange(len(data[i])):
                y = data[i][j]
                if np.max(y) > mx:
                    mx = np.max(y)
                if np.min(y) < mn and np.min(y) != 0:
                    mn = np.min(y)
            mx = max((enc[i][0][0] + enc[i][1][0] + enc[i][2][0]), mx)
        
        mn = 10**(np.ceil(np.log10(mn)) - 1.0)
        mx = 10**(np.floor(np.log10(mx)) + 1.0)
        
        for i in xrange(6):
            ax = plt.subplot(nums[i])
            ax.set_yscale('log')
            ax.set_ylim([mn, mx])
            for j in xrange(len(data[i])):
                y = data[i][j]
                col = cols[j]
                ax.plot(qs, y, '-', color=col)
            ax.plot(qs, (enc[i][0][0] + enc[i][1][0] + enc[i][2][0]) *np.ones(qnum), '--', color='red')
            if (nums[i] in [321, 323, 325]):
                plt.ylabel('Response Time(secs)')
            if (nums[i] in [325, 326]):
                plt.xlabel('Query Sequence')

            if (nums[i] in [321,323]):
                ax.set_xticklabels([])
                ax.yaxis.get_major_ticks()[1].label1.set_visible(False)
            
            if (nums[i] in [322,324]):
                ax.set_xticklabels([])
                ax.set_yticklabels([])
                
            if (nums[i] == 326):
                ax.set_yticklabels([])
                ax.xaxis.get_major_ticks()[0].label1.set_visible(False)
            
            ax.text(0.5, 0.85, texts[i], ha='center', va='center', transform=ax.transAxes, size='large', weight='bold')
                
            
        lgd = fig.legend(lines, labels, loc=8, ncol=4, bbox_to_anchor = (0.025,-0.06,1,1), 
                         bbox_transform = plt.gcf().transFigure )
        plt.subplots_adjust(wspace=0, hspace=0)
        plt.savefig(gdr + name, bbox_extra_artists=(lgd,), bbox_inches='tight', dpi=300)



def figure_11_prepare():
    qnum = Q
    qs = np.arange(1, qnum + 1)
    plain = []
    enc = []
    ambig = []
    scan = []
    Ns = [k*M1 for k in [32, 16, 4, 1]]
    for N in Ns:
        plain.append(get_times(N, 4, 'Random_1e-2_{0}_plain'.format(qnum))[-1])
        enc.append(get_times(N, 4, 'Random_1e-2_{0}_enc'.format(qnum))[-1])
        ambig.append(get_times(N, 4, 'Random_1e-2_{0}_ambig'.format(qnum))[-1])
        assert(plain[-1].shape[0] == enc[-1].shape[0] == ambig[-1].shape[0])
    return plain, enc, ambig
        
def figure_11(plain, enc, ambig):
    qnum = Q
    qs = np.arange(1, qnum + 1)
    handles=[]
    handles.append(mlines.Line2D([], [], color='red', linestyle='-',
                          markersize=2))
    handles.append(mlines.Line2D([], [], color='lime', linestyle='-',
                          markersize=2))
    handles.append(mlines.Line2D([], [], color='blue', linestyle='-',
                          markersize=2))
    handles.append(mlines.Line2D([], [], color='violet', linestyle='-',
                          markersize=2))
    handles.append(mlines.Line2D([], [], color='violet', linestyle=':',
                          markersize=2))
    handles.append(mlines.Line2D([], [], color='blue', linestyle=':',
                          markersize=2))
    handles.append(mlines.Line2D([], [], color='lime', linestyle=':',
                          markersize=2))
    handles.append(mlines.Line2D([], [], color='red', linestyle=':',
                          markersize=2))
    labels = ['32M', '16M', '4M', '1M', 'SecureScan-1M', 'SecureScan-4M', 'SecureScan-16M', 'SecureScan-32M']    
    
    cols = ['violet', 'blue', 'lime', 'red']
    cols = cols[::-1]
    data = [plain, enc, ambig]
    nums = [311, 312, 313]
    fig = plt.figure(figsize=(6.5, 6.5))
    
    mn = np.inf
    mx = -np.inf
    for i in xrange(3):
        for j in xrange(len(data[i])):
            y = data[i][j]
            if np.max(y) > mx:
                mx = np.max(y)
            if np.min(y) < mn and np.min(y) != 0:
                mn = np.min(y)
            mx = max(enc[j][0], mx)

    mn = 10**(np.ceil(np.log10(mn)) - 1.0)
    mx = 10**(np.floor(np.log10(mx)) + 1.0)
    mn = mn if mn > 10**(-6) else 10**(-6)
    
    for i in xrange(3):
        ax = plt.subplot(nums[i])            
        for j in xrange(len(data[i])):
            y = data[i][j]
            col = cols[j]
            plt.plot(qs, y, '-', color=col)
            plt.plot(qs, enc[j][0]*np.ones(qnum), ':', color=col)
        plt.yscale('log')
        ax.set_ylim([mn, mx])
        plt.ylabel('Response Time(secs)')
        
        if i == 0:
            ax.text(0.5, 0.85,'Plain', ha='center', va='center', transform=ax.transAxes, size='large', weight='bold')
            ax.yaxis.get_major_ticks()[1].label1.set_visible(False)
        elif i == 1:
            ax.text(0.5, 0.85,'Encrypted', ha='center', va='center', transform=ax.transAxes, size='large', weight='bold')
            ax.yaxis.get_major_ticks()[1].label1.set_visible(False)
        elif i == 2:
            ax.text(0.5, 0.85,'Encrypted with Ambiguity', ha='center', va='center', transform=ax.transAxes, size='large', weight='bold')
        
        if i == 2:
            plt.xlabel('Query Sequence')
        else:
            ax.set_xticklabels([])
            
    plt.subplots_adjust(wspace=0, hspace=0)
    lgd = fig.legend(handles, labels, loc=8, ncol=3, bbox_to_anchor = (0.025,-0.14,1,1), 
                         bbox_transform = plt.gcf().transFigure )
    # plt.show()
    plt.savefig(gdr + 'crackTime.png', bbox_extra_artists=(lgd,), bbox_inches='tight', dpi=300)


def figure_12_prepare():
    qnum = Q
    enc = []
    N = M2
    Ls = [64, 32, 16, 8, 4]
    for L in Ls:
        enc.append(get_times(N, L, 'Random_1e-2_{0}_enc'.format(qnum))[-1])
    return enc
        
def figure_12(enc):
    qnum = Q
    qs = np.arange(1, qnum + 1)
    Ls = [64, 32, 16, 8, 4]
    
    cols = ['red', 'lime', 'blue', 'violet', 'cyan']
    labels = [ 'keySize({0})'.format(L) for L in Ls]
    
    plt.figure()
    mn = np.inf
    mx = -np.inf
    for y, col, lbl in zip(enc, cols, labels):
        plt.plot(qs, y, '-', color=col, label=lbl)
        if np.min(y) < mn and np.min(y) != 0:
                mn = np.min(y)
        if np.max(y) > mx:
                mx = np.max(y)
    mn = 10**(np.ceil(np.log10(mn)) - 1.0)
    mx = 10**(np.floor(np.log10(mx)) + 1.0)
    plt.ylim([mn, mx])
    plt.yscale('log')  
    plt.xlabel('Query Sequence')
    plt.ylabel('Response Times(secs)')
    plt.legend()
    plt.savefig(gdr + 'KeySizeData.png', bbox_inches='tight', dpi=300)


def get_vals(N, l):
    dr = "data/" + str(N) + "/" + str(l)
    vals = read_floats(dr + "/v_real.txt", gz=False)
    return vals

def get_times_client(N, l):
    dr = "res/" + str(N) + "/" + str(l)
    f = '/client_t_RandomIncSelectivity_1e-2_1000_'
    t_enc = read_floats(dr + f + 'enc.gz')
    t_ambig = read_floats(dr + f +'ambig.gz')
    assert t_enc.shape[0] == t_ambig.shape[0]
    return t_enc, t_ambig

def get_fp_rates(N, l, vals):
    dr = "res/" + str(N) + "/" + str(l)
    f = '/result_a_b_count_RandomIncSelectivity_1e-2_1000_'
    fp_rates_plain, pp_counts, query_sizes = get_fp_rate(dr + f + 'plain.gz', vals)
    fp_rates_enc, _, _ = get_fp_rate(dr + f + 'enc.gz', vals)
    fp_rates_ambig, _, _ = get_fp_rate(dr + f + 'ambig.gz', vals)
    
    assert fp_rates_plain.shape[0] == fp_rates_enc.shape[0] == fp_rates_ambig.shape[0]
    return fp_rates_plain, fp_rates_enc, fp_rates_ambig, pp_counts, query_sizes

def get_fp_rate(f, vals):
    fp_rates = []
    pp_counts = []
    query_sizes = []
    
    with gzip.open(f,'r') as fin:
        for line in fin:
            data = line.rstrip('\n')
            if len(line) == 0:
                break
        
            a, b, returned_count = data.split('_')
            a = int(a)
            b = int(b)
            returned_count = int(returned_count)
        
            ind1 = np.where(vals >= a)[0]
            ind2 = np.where(vals[ind1] <= b)[0]
            pp_count = ind2.shape[0]
            pp_counts.append(pp_count)
            query_sizes.append(b-a)
            fp_rates.append( (returned_count - pp_count) / returned_count * 100 if returned_count != 0 else 0 )
    return np.array(fp_rates), np.array(pp_counts), np.array(query_sizes)

def figure_13_prepare():
    qnum = 10**3
    N = M2
    l = 4
    t_enc, t_ambig = get_times_client(N, l)
    vals = get_vals(N, l)
    fp_rates_plain, fp_rates_enc, fp_rates_ambig, _, _ = get_fp_rates(N, l, vals)
    return t_enc, t_ambig, fp_rates_plain, fp_rates_enc, fp_rates_ambig
        
def figure_13(t_en, t_am, fp_pl, fp_en, fp_am):
    
    fig = plt.figure(figsize=(16,6), facecolor='white')
    plt.tick_params(axis='both', which='major', labelsize=24)
    plt.plot(t_en, '-b', label='Encrypted')
    plt.plot(t_am, '-r', label='Ambiguous')
    plt.xlabel('Query Sequence', fontsize=28)
    plt.ylabel('Client time, secs', fontsize=28)
    plt.yscale('log')
    plt.ylim([0, 20])
    plt.legend(fontsize = 28, bbox_to_anchor = [0.32, 1.01])
    plt.savefig(gdr + 'client_time_1M_1000queries_Random_ProgressiveSelectivity_even.png', bbox_inches='tight')

    fig = plt.figure(figsize=(16,6), facecolor='white')
    plt.tick_params(axis='both', which='major', labelsize=24)
    plt.plot(fp_am, '-r', label='Ambiguous')
    plt.xlabel('Query Sequence', fontsize=28)
    plt.ylabel('False positives rate, (%)', fontsize=28)
    plt.ylim([0,100])

    plt.legend(fontsize = 28)
    plt.savefig(gdr + 'false_positives_1M_1000queries_Random_ProgressiveSelectivity_even.png', bbox_inches='tight')
    

def main():
    call(['mkdir', '-p', gdr])
    figure_6(*figure_6_prepare())
    figure_7(*figure_7_prepare())
    figure_8_9_10(*figure_8_9_10_prepare())
    figure_11(*figure_11_prepare())
    figure_12(figure_12_prepare())
    figure_13(*figure_13_prepare())

if __name__ == "__main__":
    main()


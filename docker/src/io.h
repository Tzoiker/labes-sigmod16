#pragma once

#include "stdafx.h"

using namespace std;

/*
* Class for writing data in compressed format
*/

class GzWriter { 
public:
	gzFile f;
	GzWriter(const char *const fn);
	~GzWriter();
	void printf(const char *fmt, ...);
};

int count_lines_in_file(string path);

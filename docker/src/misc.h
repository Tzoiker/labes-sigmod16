#pragma once

#include "stdafx.h"
using namespace std;

//Helper methods for string conversion, printing, etc.

//DECLARATIONS

void tokenize(const string& str, vector<string>& tokens, const string& delimiters = "_");
void vector_from_string(const string, vector<mpf_class> &v);
void vector_from_string(const string, vector<double> &v);
void scalar_from_string(string s, mpf_class &v);
void scalar_from_string(string s, double &v);

string to_string_wprec(const mpf_class &mpf, const int prec = PRECISION_DEC);
string to_string_wprec(const double d, const int prec = PRECISION_DEC);
string to_string_wprec(const int i, const int prec = PRECISION_DEC);
string to_string_wprec(const vector<mpf_class> &v, const int prec = PRECISION_DEC);
string to_string_wprec(const vector<double> &v, const int prec = PRECISION_DEC);

long to_integer(mpf_class &v);
long to_integer(double &v);


template<typename T>
void print_matrix(vector<vector<T> > &M);

double tan(mpf_class v);

mpf_class round(mpf_class &v);


//TEMPLATE DEFINITIONS

template<typename T>
void print_matrix(vector<vector<T> > &M) {
	cout << "print_matrix: size " << M.size() << endl; 
	for(int i = 0; i < M.size(); i++) {
		for(int j = 0; j < M[i].size(); j++)
			cout << to_string_wprec(M[i][j]) << "   ";
		cout << endl;
	}
}


#pragma once


#include "workload.h"
#include "LAbESEncryptor.h"
#include "Crackers.h"
#include "CrackerColumn.h"
#include "benchmarks.h"

using namespace std;

/*
* Encryption mode is either plain(no encryption), or enc (encrypted using LAbES), or ambig (encrypted with LAbES + fake values inserted)
*/
enum EncryptionMode { Plain, Enc, Ambig };
string mode_to_string(EncryptionMode mode) {
	switch(mode) {
		case EncryptionMode::Plain: return "plain";
		case EncryptionMode::Enc: return "enc";
		case EncryptionMode::Ambig: return "ambig";
		default: assert(false);
	}
}

/*
* Tester class, template corresponds to storing format (double, mopf_class, ...).
* Test consists of cracking a column-store database as a result of numeric range-query processing
*/
template<typename T>
class LAbESTester {
private:
	const int _N, _l;
	const unsigned int _q_num;
	const int _min, _max;
	const double _selectivity;
	const string _query_workload;
	const string _data_dir;

	LAbESEncryptor<T>* _encryptor = 0;
	CrackerColumn<int>* _column_plain = 0;
	CrackerColumn<vector<T> >* _column_enc = 0;
	Workload *_workload = 0;

	bool prepare_workload(const string workload, const double selectivity, const int min, const int max);
	bool prepare_encryptor();
	bool prepare_plain();
	bool prepare_encrypted(const bool ambig);

	static void read_append_plain(string path, vector<int> &arr);
	static void read_append_enc(string path, vector<vector<T>> &arr);
	void client_work(CrackerColumn<vector<T>> const * column, const int pos_left, const int pos_right) const;
	void test(void* column, EncryptionMode mode);
public:

	LAbESTester(const int N, const int l, const string workload, const double selectivity, const unsigned int q_num, const int min = 0, const int max = INT_MAX);
	~LAbESTester();

	bool test_plain();
	bool test_enc();
	bool test_ambig();
	void validate();
};


/*
* N - dataset size
* l - key size
* workload - type of workload (Random, RandomIncSelectivity, ...)
* selectivity - selectivity of the queries
* q_num - number of queries
* min, max - values domain boundaries
* data_dir - test data must reside in a directory "data/<N>/<l>"
*/
template<typename T>
LAbESTester<T>::LAbESTester(const int N, const int l, const string workload, const double selectivity, const unsigned int q_num, const int min, const int max): _N(N), _l(l), _query_workload(workload), _selectivity(selectivity), _q_num(q_num), _min(min), _max(max), _data_dir("data/" + to_string(N) + "/" + to_string(l)) {}

template<typename T>
LAbESTester<T>::~LAbESTester() {
	if (_encryptor != 0) delete _encryptor;
	if (_column_plain != 0) delete _column_plain;
	if (_column_enc != 0) delete _column_enc;
	if (_workload != 0) delete _workload;
	_encryptor = 0; _column_plain = 0; _column_enc = 0; _workload = 0;
}

/*
* Prepare LAbES encryptor by reading the saved parameteres from corresponding directory
*/
template<typename T>
bool LAbESTester<T>::prepare_encryptor(){
	cout << "get_enc" << endl;

	//Read encryptor parameters
	string line, path = _data_dir + "/Key.txt";
	ifstream enfile (path);
	int l = 0;
	vector<T> lines;
	int noise_size = 0, total_size = 0, first_pos = 0, second_pos = 0;   
	if (enfile.is_open()){
		while ( getline (enfile,line) )
		{
			if(l == 0){
				noise_size = stoi(line); 
				total_size = noise_size + 2;
				lines.resize(total_size*total_size*4 + noise_size); 
			}
			else if(l == 1){ first_pos = stoi(line); }
			else if(l == 2){ second_pos = stoi(line); }
			else {scalar_from_string(line, lines[l-3]); }
			l++;  
		}	  
		cout << "closing" << endl;
		enfile.close();      
	} else 
	{
		cerr << "Could not open file: \"" << path << "\"" << endl;
		return false;	
	}

	if (l < total_size*total_size*4 + noise_size + 3) {
		cerr << "Key file is too small: \"" << path << "\"" << endl;
		return false;
	}
	assert(total_size == _l);
	cout << "get_enc: read lines" << endl;

	vector<vector<T> > MM;
	vector<vector<T> > MMt; 
	vector<vector<T> > MMinv; 
	vector<vector<T> > MMtinv;
	MM.resize(total_size, vector<T>(total_size, T(0)));
	MMt.resize(total_size, vector<T>(total_size, T(0)));
	MMinv.resize(total_size,vector<T>(total_size, T(0))); 
	MMtinv.resize(total_size,vector<T>(total_size, T(0)));     	
	vector<T> UU(noise_size, T(0.0));

	//Parse encryptor parameters
	l = 0;
	for(int i = 0; i < noise_size; i++)
		UU[i] = lines[l++];
	for(int i = 0; i < total_size; i++)
		for(int j = 0; j < total_size; j++)
			MM[i][j] = lines[l++];
	for(int i = 0; i < total_size; i++)
		for(int j = 0; j < total_size; j++)
			MMt[i][j] = lines[l++];
	for(int i = 0; i < total_size; i++)
		for(int j = 0; j < total_size; j++)
			MMinv[i][j] = lines[l++];
	for(int i = 0; i < total_size; i++)
		for(int j = 0; j < total_size; j++)
			MMtinv[i][j] = lines[l++];
    
	cout << "get_enc: assigned M" << endl;
	cout << "noise_size " << noise_size << endl;
	cout << "first_pos " << first_pos << endl;
	cout << "second_pos " << second_pos << endl;

	_encryptor = new LAbESEncryptor<T>(noise_size, first_pos, second_pos, true, false);

	cout << "encryptor " << (_encryptor != 0) << endl;

	_encryptor->enable_ambiguity(false);
	_encryptor->setU(UU);
	print(UU, cout);  
	_encryptor->setM(MM);
	_encryptor->setMt(MMt);
	_encryptor->setMinv(MMinv);
	_encryptor->setMtinv(MMtinv);

	cout << "return" << endl;

	print_matrix(MM);

	return true;
}

/*
* Reads plain data from path text file and appends to arr
*/
template<typename T>
void LAbESTester<T>::read_append_plain(string path, vector<int> &arr) {
	cout << "read_append_plain: started" << endl;
	ifstream fin (path, ios::in);
	if (fin.is_open()){
		cout << "read_append_palin: opened: " << path << endl;
		string line;
		while ( getline (fin,line) )
			arr.push_back(stoi(line));
		cout << "read_append_plain: closing: " << path << endl;
		fin.close();      
	} else 
	{
		cerr << "read_append_plain: could not open file: " << path << endl;
		exit(0);
	}
}

/*
* Reads encrypted data from path text file and appends to arr
*/
template<typename T>
void LAbESTester<T>::read_append_enc(string path, vector<vector<T>> &arr) {
	cout << "read_append_enc: started" << endl;
	ifstream fin (path, ios::in);
	if (fin.is_open()){
		cout << "read_append_enc: opened: " << path << endl;
		string line;
		while ( getline (fin,line) )
		{
			vector<T> Ev;
			vector_from_string(line, Ev);
			arr.push_back(Ev);
		}	  
		cout << "read_append_enc: closing: " << path << endl;
		fin.close();      
	} else 
	{
		cerr << "read_append_enc: could not open file: " << path << endl;
		exit(0);
	}
}

/*
* "Simulation" of the client workload, i.e. decryption of the qualified tuples
*/
template<typename T>
void LAbESTester<T>::client_work(CrackerColumn<vector<T>> const * column, const int pos_left, const int pos_right) const {
	assert(_encryptor != 0);
	if (pos_right < pos_left || pos_right < 0)
		throw invalid_argument("client_work: pos_right must be >= pos_left and >= 0");
	const vector<vector<T>>& arr = column->get_immutable_arr_ref();
	int p1 = pos_left, p2 = pos_right;
	if (p1 < 0) p1 = 0;
	if (p2 >= arr.size()) p2 = arr.size() - 1;
	for (auto it = arr.begin() + p1; it != arr.begin() + p2 + 1; ++it)
		_encryptor->decrypt_int_v(*it);
}

/*
* Prepares workload class instance
*/
template<typename T>
bool LAbESTester<T>::prepare_workload(const string workload, const double selectivity, const int min, const int max) {
	unsigned int S = (int) (selectivity*(max - min));
	if (_workload != 0 )
		delete _workload;
	_workload = new Workload( workload.c_str(),  S, min, max);
	return true;
}

/*
* Prepares for tests on plain data
*/
template<typename T>
bool LAbESTester<T>::prepare_plain(){
	cout << "prepare_plain: started" << endl;
	if (_column_plain != 0) delete _column_plain;
	vector<int> arr_v;
	string path = _data_dir + "/v_real.txt";
	LAbESTester::read_append_plain(path, arr_v);
	_column_plain = new CrackerColumn<int>(arr_v);
	cout << "prepare_plain: finished" << endl;
	return true;
}

/*
* Prepares for tests on encrypted data
*/
template<typename T>
bool LAbESTester<T>::prepare_encrypted(const bool ambig){
	cout << "prepare_encrypted: started" << endl;
	if (_column_enc != 0) delete _column_enc;
	vector<vector<T>> arr_Ev;
	string path = _data_dir + "/Ev_real.txt";
	LAbESTester<T>::read_append_enc(path, arr_Ev);

	if (ambig) {
		string path = _data_dir + "/Ev_fake.txt";
		LAbESTester<T>::read_append_enc(path, arr_Ev);
	}
	_column_enc = new CrackerColumn<vector<T>>(arr_Ev);
	cout << "prepare_encrypted: finished" << endl;
	return true;
}

/*
* Launches tests on plain data
*/
template<typename T>
bool LAbESTester<T>::test_plain() {
	if (!prepare_plain()) return false;
	test((void *)_column_plain,  EncryptionMode::Plain);
	return true;
}

/*
* Launches tests on encrypted data
*/
template<typename T>
bool LAbESTester<T>::test_enc() {	
	if (!prepare_encryptor()) return false;
	if (!prepare_encrypted(false)) return false;
	test((void *)_column_enc,  EncryptionMode::Enc);
	return true;
}


/*
* Launches tests on ambiguous data
*/
template<typename T>
bool LAbESTester<T>::test_ambig() {	
	if (!prepare_encryptor()) return false;
	if (!prepare_encrypted(true)) return false;
	test((void *)_column_enc,  EncryptionMode::Ambig);
	return true;
}

/*
* Launches tests on data
*/
template<typename T>
void LAbESTester<T>::test(void* column, EncryptionMode mode) {
	cerr << "Testing: N = " << setprecision(2) << fixed << _N / 1000000.0 << "M, l = " << _l << ", D = [" << _min << ", " << _max << "], workload = " << _query_workload;
	cerr.unsetf ( ios::floatfield  );
	cerr << setprecision(6) << ", selectivity = " << _selectivity << ", Qnum = " << _q_num << ", mode = " <<  mode_to_string(mode) << endl;


	if (!prepare_workload(_query_workload, _selectivity, _min, _max))
		throw runtime_error("LAbESTester: prepare_workload: failed!");


	Timer client_t;
	//Prepare output files for results
	GzWriter result_a_b_count_f(string("result_a_b_count.gz").c_str());
	GzWriter n_touched_f(string("n_touched.gz").c_str());
    GzWriter treeSearch_t_f(string("treeSearch_t.gz").c_str());
    GzWriter crack_t_f(string("crack_t.gz").c_str());
    GzWriter treeInsert_t_f(string("treeInsert_t.gz").c_str());
	GzWriter total_t_f(string("total_t.gz").c_str());
	GzWriter total_cumulative_t_f(string("total_cumulative_t.gz").c_str());
	GzWriter client_t_f(string("client_t.gz").c_str());

    double total_cumulative_t = 0;


    double progress = -1;
    for (int i = 0, a, b; i < _q_num; i++){

		double progress_new = i*100.0 / (_q_num-1);
		if (round(progress*100) != round(progress_new*100)) {
			progress = progress_new;
			cerr << '\r' << "Progress..." << fixed << setprecision(2) << progress << "%"; 
		}

    	client_t.clear();

        bool ok = _workload->query(a,b);

        assert(ok);
    	double time_elapsed;


    	CrackersResult res;
    	switch(mode) {
    		case EncryptionMode::Plain:
    		{
    			res = ((CrackerColumn<int> *)column)->view_query(a, a, b, b);
    		}
    		break;
    		case EncryptionMode::Enc:
    		case EncryptionMode::Ambig:
    		{
    			vector<T> a_key = _encryptor->encrypt_int_b(a);
    			vector<T> a_val = _encryptor->encrypt_int_v(a);
    			vector<T> b_key = _encryptor->encrypt_int_b(b);
    			vector<T> b_val = _encryptor->encrypt_int_v(b);
    			res = ((CrackerColumn<vector<T>>*)column)->view_query(a_key, a_val, b_key, b_val);
      			client_t.start(); client_work((CrackerColumn<vector<T>>*)column, res.pos_left, res.pos_right); client_t.stop();
    		}
    		break;
    		default: assert(false);
    	}

    	double treeSearch_t = res.search_t;
    	double crack_t = res.crack_t;
    	double treeInsert_t = res.insert_t;
    	double total_t = treeSearch_t + crack_t + treeInsert_t ;
    	total_cumulative_t += total_t;

    	//Output the results
    	result_a_b_count_f.printf("%d_%d_%d\n", a, b, res.pos_right - res.pos_left);
    	n_touched_f.printf("%d\n", res.touched);
    	treeSearch_t_f.printf("%.6lf\n", treeSearch_t);
    	crack_t_f.printf("%.6lf\n", crack_t);
    	treeInsert_t_f.printf("%.6lf\n", treeInsert_t); 
    	total_t_f.printf("%.6lf\n", total_t);
    	total_cumulative_t_f.printf("%.6lf\n", total_cumulative_t);    
    	client_t_f.printf("%.6lf\n", client_t.elapsed());
    }
    cerr << endl;
    cout << endl;
}


/*
* Auxiliary method for correctness checking (numerical instability may be an issue)
*/
template<typename T>
void LAbESTester<T>::validate() {
	int a, b;
	assert(prepare_workload(_query_workload, _selectivity, _min, _max));
	assert(prepare_encryptor());	
	assert(prepare_plain());
	assert(prepare_encrypted(false));
	cerr << "prepared plain, encrypted, encryptor" << endl;
	cerr << "col_plain " << _column_plain << "   " << "col_enc " << _column_enc << endl;
	const vector<int> &arr_plain = _column_plain->get_immutable_arr_ref();
	const vector<vector<T>> &arr_enc = _column_enc->get_immutable_arr_ref();
	assert(arr_plain.size() == arr_enc.size());
	_workload->query(a, b);
	vector<T> Eb_a = _encryptor->encrypt_int_b(a);
    vector<T> Eb_b = _encryptor->encrypt_int_b(b);
	cerr << "a = " << a << " b = " << b << endl;
	for (int i = 0; i < arr_plain.size(); i++){
		cerr << '\r' << int(100.0*i/(arr_plain.size()-1));
		vector<T> Ev = arr_enc.at(i);
		int v = arr_plain.at(i);
		if(v != _encryptor->decrypt_int_v(Ev))
			cerr << "not valid dec " << i << "    " << v << endl;
		if(v != _encryptor->decrypt_int_b(_encryptor->encrypt_int_b(v)))
				cerr << "not valid dec b " << endl;
		if ( !_encryptor->validate(Ev, Eb_a, v, a)) 
			cerr << "not valid cmp a, i =  " << i << " v = " << v << " a = " << a << endl;
		if ( !_encryptor->validate(Ev, Eb_b, v, b)) 
            cerr << "not valid cmp b, i =  " << i << " v = " << v << " b = " << b << endl;

	}
}

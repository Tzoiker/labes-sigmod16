#pragma once

#include "stdafx.h"

//Random generator for LAbES purposes
template <typename T>
class LAbESRandom
{
public:
	static T getValue(int mode);
};

template <typename T>
T LAbESRandom<T>::getValue(int mode)
{
	T v1 = 0.2, v2 = 4.8, v3 = 1000, v4 = 2, v5 = 4, value = 0;        
			   if (mode == 0){
				value = (v1 + (v2*(rand() % 1000)/v3));
                                return value;   
			   }
			   if (mode == 1){
				value = (v4 - (v5 *(rand() % 1000)/v3));
                                return value; 
			   }
			  else
			   throw std::invalid_argument("Wrong random mode");
       return value;		
}
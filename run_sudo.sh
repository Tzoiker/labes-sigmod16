#!/usr/bin/env bash
cd docker;
docker build -t labes .;
docker run labes;
PUID=$(docker ps -a | awk '/labes/{print $1}' | awk 'NR==1{print}');
docker cp ${PUID}:/home/latex/securadapt.pdf ../;
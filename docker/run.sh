#!/usr/bin/env bash
if (( $# < 1 )); then
    echo "Usage: [dataset scale]"
    exit 1
fi
python gen_data.py $1;
python test.py $1;
python gen_graphs.py $1;
mv graphs/* latex/Figures/;
rm -r graphs;
cd latex;
pdflatex -synctex=1 -interaction=nonstopmode securadapt.tex;
pdflatex -synctex=1 -interaction=nonstopmode securadapt.tex;
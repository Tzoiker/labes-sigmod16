#include "benchmarks.h"

double timing() {
	static struct timeval t1, t2;
	gettimeofday(&t2,NULL);
	double ret = t2.tv_sec - t1.tv_sec + (t2.tv_usec - t1.tv_usec) * 1e-6;
	t1 = t2;
	return ret;
}
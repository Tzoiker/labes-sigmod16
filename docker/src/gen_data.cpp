
#include <stdio.h>
#include <assert.h>
#include <algorithm>
#include "random.h"
#include "LAbESEncryptor.h"
#include "stdafx.h"
#include "string.h"
#include "data_generator.h"
#include<fstream>
using namespace std;


/** arguments
1. Dataset size
2. Secret key size
3. Domain left bound
4. Domain right bound
5. Generate fake values? 0 or 1
6. Use random seed for secret key? 0 or 1 (should be 0 to ensure correctness)
7. Use random seed for generation of values? 0 or 1
*/
int main(int argc, char *argv[]){
	assert(argc == 8);
	int N, l, mn, mx; 
	bool generate_fake, random_key, random_values;

	N = atoi(argv[1]);
	l = atoi(argv[2]);
	mn = int(stod(string(argv[3])));
	mx = int(stod(string(argv[4])));
	generate_fake = atoi(argv[5]);
	random_key = atoi(argv[6]);
	random_values = atoi(argv[7]);

	//Set default precision fro mpf_class
	mpf_set_default_prec(PRECISION_BITS);
	DataGenerator<mpf_class>* generator = new DataGenerator<mpf_class>(N, l, mn, mx, generate_fake, random_key, random_values); 
	generator->save_key();
	generator->generate();
}

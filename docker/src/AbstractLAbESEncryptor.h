#pragma once

#include "AbstractEncryptor.h"


/*
* Abstract numeric data encryptor based on LAbES structure
*/
template <typename T> class AbstractLAbESEncryptor : public AbstractEncryptor<T>
{
protected:
	virtual T normalize_v(T v) = 0;
	virtual vector<T> vectorize_v(T  v) = 0;
	virtual vector<T> scale_v(vector<T> Ev) = 0;
	virtual vector<T> add_noise_v(vector<T> Ev) = 0;
	virtual vector<T> tangle_v(vector<T> Ev) = 0;
	virtual vector<T> add_ambiguity_v(vector<T> Ev) = 0;
	virtual vector<T> resolve_ambiguity_v(vector<T> Ev) = 0;
	virtual vector<T> untangle_v(vector<T> Ev) = 0;
	virtual vector<T> remove_noise_v(vector<T> Ev) = 0;
	virtual T scalarize_v(vector<T> Ev) = 0;
	virtual T denormalize_v(T v) = 0;

	virtual T normalize_b(T b) = 0;
	virtual vector<T> vectorize_b(T  b) = 0;
	virtual vector<T> scale_b(vector<T> Eb) = 0;
	virtual vector<T> add_noise_b(vector<T> Eb) = 0;
	virtual vector<T> tangle_b(vector<T> Eb) = 0;
	virtual vector<T> untangle_b(vector<T> Eb) = 0;
	virtual vector<T> remove_noise_b(vector<T> Eb) = 0;
	virtual T scalarize_b(vector<T> Eb) = 0;
    virtual T denormalize_b(T b) = 0;

    int m_total_size;
	int m_noise_size;
	int m_first_pos;
	int m_second_pos;
	bool m_ambig_end;


public:
      
    void enable_normalization(bool b) { this->normalize = b; }
	void enable_scale(bool b){ this->scale = b; }
	void enable_noise(bool b){ this->noise = b; }
	void enable_tangle(bool b){ this->tangle = b; }
	void enable_ambiguity(bool b){ this->ambiguity = b; }

	bool is_normalization_enabled() { return this->normalize; }
	bool is_scale_enabled(){ return this->scale; }
	bool is_noise_enabled(){ return this->noise; }
	bool is_tangle_enabled(){ return this->tangle; }
	bool is_ambiguity_enabled(){ return this->ambiguity; }

	vector<T> encrypt_v(T v)
	{
		if (normalize)
			v = normalize_v(v);
		vector<T> Ev = vectorize_v(v);
		if (noise)
			Ev = add_noise_v(Ev);
		if (scale)
			Ev = scale_v(Ev);
		if (tangle)
			Ev = tangle_v(Ev);
		if (ambiguity)
			Ev = add_ambiguity_v(Ev);
		return Ev;
	}
	vector<T> encrypt_b(T b)
	{
		if (normalize)
			b = normalize_b(b);
		vector<T> Eb = vectorize_b(b); 
		if (noise)
			Eb = add_noise_b(Eb);   
		if (scale)
			Eb = scale_b(Eb);
		if (tangle)
			Eb = tangle_b(Eb);
		return Eb;
	}
	T decrypt_v(vector<T> Ev)
	{ 
		if (ambiguity && (int)Ev.size() == m_total_size + (int)1)
			Ev = resolve_ambiguity_v(Ev);
		else if (ambiguity && (int)Ev.size() != m_total_size)
			std::invalid_argument("Invalid size of Ev");
                cout.flush();
		if (tangle)
			Ev = untangle_v(Ev);
		if (noise)
			Ev = remove_noise_v(Ev);		
		T v = scalarize_v(Ev);
		if (normalize)
			v = denormalize_v(v);
		return v;
	}
	T decrypt_b(vector<T> Eb)
	{
		if (tangle)
			Eb = untangle_b(Eb);
		if (noise)
			Eb = remove_noise_b(Eb);
		T b = scalarize_b(Eb);
		if (normalize)
			b = denormalize_b(b);
		return b;
	}

private:
	bool normalize = true;
	bool noise = true;
	bool scale = true;
	bool tangle = true;
	bool ambiguity = true;	

};

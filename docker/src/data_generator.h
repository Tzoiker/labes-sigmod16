#include <stdio.h>
#include <assert.h>
#include <algorithm>
#include "random.h"
#include "LAbESEncryptor.h"
#include "stdafx.h"
#include "string.h"
using namespace std;


//Data generator class
template<typename T>
class DataGenerator
{
public:
	
	DataGenerator(const int dataset_size, const int key_size, const int min = INT_MIN, const int max = INT_MAX, const bool generate_fake = true, const bool random_key = true, const bool random_values = true);

	void generate();
	void save_key();

private:

	void generate_real();
	void generate_fake();
	void generate_data(LAbESEncryptor<T>* encryptor, string plaintexts, string ciphertexts, long seed);
	

	const int _N, _l, _min, _max;
	const bool _generate_fake, _random_key, _random_values;

	LAbESEncryptor<T>* encryptor;
};


//DEFINITIONS
/*
* dataset_size - N
* key_size - l
* min, max - boundaries of encrypted values
* generate_fake - generate fake values or not
* random_key - use random seed for key generation or not (to have a stable result due to possibility of numerical instability, should be false)
* random_values - use random seed for data values generation (false or true)
*/
template<typename T>
DataGenerator<T>::DataGenerator(const int dataset_size, const int key_size, const int min, const int max, const bool generate_fake, const bool random_key, const bool random_values) : _N(dataset_size), _l(key_size), _min(min), _max(max), _generate_fake(generate_fake), _random_key(random_key), _random_values(random_values) {
	if (key_size < 4)
		throw std::invalid_argument("Wrong noise size");
	if (dataset_size < 1)
		throw std::invalid_argument("Wrong dataset size");

	//Theta generation technique is not good in terms of security (but can be used to save storage), 
	//and also increases the encryption process (in order to drop bad fake encrypted values).
	//Thus, in another paper, we proposed to generate fake values by picking fake values not deterministically, 
	//but from some distribution.
	//This modification does not affect the experimental results, but helps to simplify the process
	//and increase the speed of data generation.
	if(!random_key) {
		encryptor = new LAbESEncryptor<T>(_l-2, 0, 1, true, true, 1444086458, 1444452077);
	} else {
		encryptor = new LAbESEncryptor<T>(_l-2, 0, 1, true, true, time(0), time(0));
	}
	encryptor->enable_ambiguity(false);
}



template<typename T>
void DataGenerator<T>::generate() {
	generate_real();
	if (_generate_fake)
		generate_fake();
}

template<typename T>
void DataGenerator<T>::generate_real() {
	string pltxt = "v_real.txt";
	string cptxt = "Ev_real.txt";
	generate_data(encryptor, pltxt, cptxt, _random_values ? time(0) : 140384);
}

template<typename T>
void DataGenerator<T>::generate_fake() {
	string pltxt = "v_fake.txt";
	string cptxt = "Ev_fake.txt";
	generate_data(encryptor, pltxt, cptxt, _random_values ? time(0) : 140385);
}

//Data is generated randomly, with values uniformly distributed over the domain of values ([0, 2^31] in the paper)
template<typename T>
void DataGenerator<T>::generate_data(LAbESEncryptor<T>* encryptor, string pltxt, string cptxt, long seed) {
	cerr << "Generating data: N = " << setprecision(2) << fixed << _N / 1000000.0 << "M, l = " << _l << ", D = [" << _min << ", " << _max << "]: "  << pltxt << ", " << cptxt << endl;

	Random r(seed);

	ofstream ofs_pl(pltxt.c_str());  
	ofstream ofs_cp(cptxt.c_str());  
	
	double progress = -1;

	for (int i = 0; i < _N; i++) {

		double progress_new = i*100.0 / (_N-1);
		if (round(progress*100) != round(progress_new*100)) {
			progress = progress_new;
			cerr << '\r' << "Progress..." << fixed << setprecision(2) << progress << "%"; 
		}

		double rnd = r.nextNormedToZeroOne();
		int v = (int)( rnd*(_max - _min) + _min );
		vector<T> Ev = encryptor->encrypt_int_v(v);

		assert(v == encryptor->decrypt_int_v(Ev));

		ofs_pl << to_string_wprec((int) v) << endl;
		ofs_cp << to_string_wprec(Ev) << endl;
	}
	cerr << endl;
	ofs_pl.close();
	ofs_cp.close();
}

//Dumps the ecnryptor info to the text file
template<typename T>
void DataGenerator<T>::save_key() {
	cout<<"Saving secret key:"<<endl;

	string file = string("Key") + ".txt";
	cout<<"Saving secret key: file: " << file <<endl;
	ofstream ofEnc;
	ofEnc.open(file.c_str());
	ofEnc << to_string_wprec(encryptor->getNoiseSize())<<"\n";
	ofEnc << to_string_wprec(encryptor->getFirstPos())<<"\n";
	ofEnc << to_string_wprec(encryptor->getSecondPos())<<"\n";
	vector<vector<T> > M = encryptor->getM();
	vector<vector<T> > Mt = encryptor->getMt(); 
	vector<vector<T> > Minv = encryptor->getMinv(); 
	vector<vector<T> > Mtinv = encryptor->getMtinv();   	
	vector<T> U = encryptor->getU();
	for(int i=0;i<encryptor->getNoiseSize();i++){
		ofEnc << to_string_wprec(U[i])<<"\n";
	}  
	cout<<"prinitng M:"<<endl;
	for(int i=0;i<encryptor->getSize();i++){
		for(int j=0;j<encryptor->getSize();j++){
			cout<<M[i][j]<<"    ";  
			ofEnc << to_string_wprec(M[i][j])<<"\n";
		}
		cout<<endl;		
	}
	for(int i=0;i<encryptor->getSize();i++){
		for(int j=0;j<encryptor->getSize();j++){
			ofEnc << to_string_wprec(Mt[i][j])<<"\n";
		}		
	}   

	for(int i=0;i<encryptor->getSize();i++){
		for(int j=0;j<encryptor->getSize();j++){
			ofEnc << to_string_wprec(Minv[i][j])<<"\n";
		}		
	}
	for(int i=0;i<encryptor->getSize();i++){
		for(int j=0;j<encryptor->getSize();j++){
			ofEnc << to_string_wprec(Mtinv[i][j])<<"\n";
		}		
	}
	ofEnc.close();
}


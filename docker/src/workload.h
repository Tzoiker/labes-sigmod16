#pragma once

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <algorithm>
#include "random.h"
#include <climits>
#include <iostream>

/*
* Range query generator
*/
class Workload {
	const int _min;	//Minimum possible value
	const int _max;	//Maximum possible value
	int W;    // the selected workload to be generated
	int S;    // the selectivity (unused for some workloads)
	int I;    // the I'th query (internal use only)
	int a, b; // the last query range [a,b]
	Random r; // Pseudo Random Generator

	int get_min() { return _min; }
	int get_max() { return _max; }
	bool random_w();
	bool random_increase_selectivity();

public : 
	Workload(const char *workload, const int selectivity, const int min, const int max);
	bool query(int &na, int &nb);

};

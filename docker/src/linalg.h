#pragma once

//Functions for comparisons and linear algebraic operations

//DECLARATIONS

#include "stdafx.h"

using namespace std;

template<typename T>
static vector<T> VectorMulScalar(vector<T> Vec, T scalar);

template<typename T>
T VectorMulVectorScalar(const vector<T> &Vec1, const vector<T> &Vec2);

template<typename T>
void VectorMulVectorScalar(const vector<T> &Vec1, const vector<T> &Vec2, T &res);

template<typename T>
bool MatEqual(const vector< vector<T> > &mat1, const vector< vector<T> > &mat2);

template<typename T>
void MatrixMulVector(vector< vector<T> > Mat1,vector<T> Vec,vector<T> &VecRes);

template<typename T>
void MatrixMul(vector< vector<T> > mat1, vector< vector<T> > mat2, vector< vector<T> > &res);
template<typename T>
vector<vector<T>> MatrixMul(vector< vector<T> > mat1,vector< vector<T> > mat2);

template<typename T>
int CompareVector(const vector<T> &Ev, const vector<T> &Eb);

template<typename T>
bool CheckVectorEquality(vector<T> Ev, vector<T> Eb);

template<typename T>
bool CheckVectorLess(vector<T> Ev, vector<T> Eb);

template<typename T>
int compare(const vector<T> &Ev, const vector<T> &Eb);

template<typename T>
int compare(const T &, const T &, const double eps = COMPARISON_EPSILON);

template<typename T>
int compare_vb(const vector<T> &Ev, const vector<T> &Eb){ return compare(Ev, Eb); }
template<typename T>
int compare_bv(const vector<T> &Eb, const vector<T> &Ev){ return -compare(Ev, Eb); }

template<typename T>
int compare_vb(const T &v, const T &b, const double eps = COMPARISON_EPSILON) { return compare(v, b, eps); }
template<typename T>
int compare_bv(const T &b, const T &v, const double eps = COMPARISON_EPSILON) {return compare(b, v, eps); }


void MatrixInverse(vector<vector<double> > M, vector<vector<double> > &Minv);
void MatrixInverse(vector<vector<mpf_class> > M, vector<vector<mpf_class> > &Minv);

template<typename T>
void print(std::vector<T> v, std::ostream& stream);



//DEFINITIONS

template<typename T>
static vector<T> VectorMulScalar(vector<T> Vec, T scalar) {
	for(int i = 0;i< (int)Vec.size();i++){
		Vec[i] *= scalar;
	}
	return Vec;
}

template<typename T>
T VectorMulVectorScalar(const vector<T> &Vec1, const vector<T> &Vec2) {	
	unsigned long len = min(Vec1.size(), Vec2.size());
	T res(0.0);
	for(int i = 0; i < len; i++)
		res += Vec1[i] * Vec2[i];
	return res;
}

template<typename T>
void VectorMulVectorScalar(const vector<T> &Vec1, const vector<T> &Vec2, T &res) {	
	res = VectorMulVectorScalar(Vec1, Vec2);
}

template<typename T>
bool MatEqual(const vector< vector<T> > &mat1, const vector< vector<T> > &mat2, const unsigned int prec){
	cout.precision(PRECISION_DEC);	
	for(int i=0;i<(int)mat1.size();i++){
		for(int j=0;j<(int)mat1.size();j++){
			T t1(abs(mat1[i][j] - mat2[i][j]));
			T t2(compare(t1, T(prec)));  		
			if(t2 != 0){
				return false;
			}
		}		
	}
	return true;
}

template<typename T>
void MatrixMulVector(vector< vector<T> > Mat1,vector<T> Vec,vector<T> &VecRes){
	for(int i = 0 ; i < (int) Mat1.size(); i++){
		T sum = 0.0;
		for(int j = 0; j < (int) Mat1.size(); j++)
			sum += (Mat1[i][j] * Vec[j]);
		VecRes[i] = sum;
	}		
} 

template<typename T>
int CompareVector(const vector<T> &Ev, const vector<T> &Eb){
	T res = 0.0;
	T prec(COMPARISON_EPSILON); 
	VectorMulVectorScalar(Ev,Eb,res);
	if ( abs(res) <= prec )
		return 0;
	else
		return res < 0.0 ? -1 : 1;
} 

template<typename T>
bool CheckVectorEquality(vector<T> Ev,vector<T> Eb){
	return CompareVector(Ev, Eb) == 0;
} 

template<typename T>
bool CheckVectorLess(vector<T> Ev,vector<T> Eb){
	return CompareVector(Ev, Eb) < 0;
} 

template<typename T>
int compare(const vector<T> &Ev, const vector<T> &Eb) {
	return CompareVector(Ev, Eb);
}

template<typename T>
int compare(const T &a, const T &b, const double eps) {

	T res = a - b;
	T prec(eps); 
	if ( abs(res) <= prec ) 
		return 0;
	else 
		return res < 0.0 ? -1 : 1;
}

template<typename T>
void print(std::vector<T> v, std::ostream& stream) {
	stream.precision(PRECISION_DEC);
	for (int i = 0; i < (int)v.size(); i++)
		stream << v[i] << " ";
	stream << endl;
}

template<typename T>
void MatrixMul(vector< vector<T> > mat1,vector< vector<T> > mat2,vector< vector<T> > &res){
	T sum = 0.0;
	for (int i = 0; i < (int)mat1.size(); i++) {
		for (int j = 0; j < (int)mat1.size(); j++) {
			sum = 0;
			for (int k = 0; k < (int)mat1.size(); k++) {
				sum = sum + mat1[i][k] * mat2[k][j];
			}
			res[i][j] = sum;
		}
	}
}

template<typename T>
vector<vector<T>> MatrixMul(vector< vector<T> > mat1,vector< vector<T> > mat2){
	vector<vector<T>> res(mat1.size(), vector<T>(mat1.size()));
	T sum = 0.0;
	for (int i = 0; i < (int)mat1.size(); i++) {
		for (int j = 0; j < (int)mat1.size(); j++) {
			sum = 0;
			for (int k = 0; k < (int)mat1.size(); k++) {
				sum = sum + mat1[i][k] * mat2[k][j];
			}
			res[i][j] = sum;
		}
	}
	return res;
}
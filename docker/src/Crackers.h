#pragma once

#include <map>
#include <set>
#include <vector>
#include "AVLTree.h"


/*
* Helper class for cracking.
*/
template<typename T>
class Crackers {
public:
  //Finds a cracking piece that corresponds to the query bound b
	static void find_piece(AVLTree<T> &tree, const int N, const T &b,  int &L, int &R);

  //Partitions a cracking piece in 2 parts with respect to the query bound
	static int partition(vector<T> &arr, const T &b, const int L, const int R);

  //Partitions a cracking piece in 3 parts with respect to the query bounds
	static void split_ab(vector<T> &arr, const int L, const int R, const T &a, const T &b, int &i1, int &i2);

  //Adds a crack information into an AVL tree
	static void add_crack(AVLTree<T> &tree, const int N, const T &v, const T &b, const int p);
};


template<typename T>
int Crackers<T>::partition(vector<T> &arr, const T &b, const int L, const int R){
	int first = L;
	int last  = R;
	while (first!=last) {
   while (compare_vb(arr[first], b) < 0) {
     ++first;
     if (first==last) return first;
   }
   do {
     --last;
     if (first==last) return first;
   } while (compare_vb(arr[last], b) >= 0);
   swap (arr[first],arr[last]);
   ++first;
 }
 return first;
}

template<typename T>
void Crackers<T>::find_piece(AVLTree<T> &tree, const int N, const T &b, int &L, int &R) {
  L = 0; R = N;
  bool isFound = true;
  Node<T> *root = tree.getRoot();
  
  if(root!=0){
    Node<T> *minNode = tree.findMin(root);
    Node<T> *maxNode = tree.findMax(root);
    Node<T> *LBound =  tree.findNode(b);

    if ( compare_bv(b, maxNode->getkey()) > 0 )
      isFound = false;

    if ( !isFound && LBound != minNode )
      L = maxNode->getPos();
    else if ( LBound == minNode ) {
      if ( compare_bv(b, LBound->getkey()) < 0 )
        R = LBound->getPos();
      else {
        L = LBound->getPos();
        LBound = tree.findNextLargest(LBound, maxNode);
        if ( compare_bv(LBound->getkey(), maxNode->getkey()) < 0 )
          R = LBound->getPos();
      }
    } else if ( compare_bv(b, LBound->getkey()) < 0 ) {
      R = LBound->getPos();
      L = tree.findPrevMinimum(LBound, minNode)->getPos();
    } else {
      L = LBound->getPos();
      LBound = tree.findNextLargest(LBound,maxNode);
      if ( compare_bv(b, LBound->getkey()) < 0 )
        R = LBound->getPos();
    }
  }
}

template<typename T>
void Crackers<T>::split_ab(vector<T> &arr, const int first, const int last, const T &a, const T &b, int &i1, int &i2){
  int L = first, R = last - 1;
  i1 = L;
  while (L<=R){
    while (L<=R && compare_vb(arr[L],b) < 0){
      if (compare_vb(arr[L],a) < 0) {
        swap(arr[L], arr[i1]); i1++;
      }               
      L++;
    }
    while (L<=R && compare_vb(arr[R],b) >= 0) R--;
    if (L<R) {swap(arr[L], arr[R]);}
  }

  i2 = L;
}

template<typename T>
void Crackers<T>::add_crack(AVLTree<T> &tree, const int N, const T &v, const T &b, const int p) {
  Node<T> *root = tree.getRoot();

  if (p == 0 || p >= N) 
    return;

  bool isFound = true;

  if (root != 0) {
    Node<T> *minNode = tree.findMin(root);
    Node<T> *maxNode = tree.findMax(root);
    Node<T> *LBound =  tree.findNode(b);
    if ( compare_bv(b, maxNode->getkey()) > 0 )
      isFound = false;

    if (isFound) {
      if (LBound->getPos() == p) return;
      Node<T> *tmp = LBound;
      if ( compare_vb(tmp->getkey(), b) == 0 )
        tmp = tree.findNextLargest(tmp,maxNode);
      if ( compare_vb(tmp->getkey(), b) > 0 )
        if ( tmp->getPos() == p ) return;
    } else if ( LBound != minNode ) {
      if (!isFound) 
        LBound = maxNode;
      else 
        LBound =tree.findPrevMinimum(LBound,minNode);
      if (LBound->getPos() == p) return;
    } else if (isFound && compare_vb(LBound->getkey(), b) == 0) {
      LBound->setPos(p); return;
    } 
  }

  tree.insert(v, b ,p);
}

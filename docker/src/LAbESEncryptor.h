#pragma once

#include "LAbESRandom.h"
#include "AbstractLAbESEncryptor.h"
#include "stdafx.h"

/*
* Encryptor class that implements LAbES functionality.
*/
template<typename T>
class LAbESEncryptor : public AbstractLAbESEncryptor<T>
{
public:
	/**
	@param noise_size size of noise, that will be added.
	@param first_pos position of v (if value) or 1 (if query) in encrypted vector
	@param second_pos position of -1 (if value) or b (if query) in encrypted vector
	@param ambig_end if true, then fake value q appeneded to the end of vector, if false - to beginning
	*/
	LAbESEncryptor(const int noise_size, const int first_pos, const int second_pos, const bool ambig_end = true, 
		const bool prepare_key = true, const long u_seed = 1444086458, const long m_seed = 1444452077);
	LAbESEncryptor(LAbESEncryptor<T> encr, const bool ambig_end);
	LAbESEncryptor(LAbESEncryptor<T> encr, const int first_pos, const int second_pos, const bool ambig_end);
	~LAbESEncryptor() {};
	int getSize() { return this->m_total_size; }
	int getNoiseSize() { return this->m_noise_size; }
	int getFirstPos(){ return this->m_first_pos; }
	int getSecondPos(){ return this->m_second_pos; }
	bool getAmbigEnd(){return this->m_ambig_end;} 
	vector<T> getU(){ return U; }
	vector<vector<T> > getM(){ return this->M; }
	vector<vector<T> > getMt(){ return this->Mt; }
	vector<vector<T> > getMinv(){ return this->Minv;}
	vector<vector<T> > getMtinv(){ return this->Mtinv;}
	void setU(vector<T> U){ this->U = U; }
	void setM(vector<vector<T> > M){ this->M = M; }
	void setMt(vector<vector<T> > Mt){ this->Mt = Mt; }
	void setMinv(vector<vector<T> > Minv){ this->Minv = Minv; }
	void setMtinv(vector<vector<T> > Mtinv){ this->Mtinv = Mtinv; }

	/*
	Encrypt / decrypt methods for integer types
	*/
	vector<T> encrypt_int_v(long v);
	long decrypt_int_v(vector<T> Ev);
	vector<T> encrypt_int_b(long b);
	long decrypt_int_b(vector<T> Eb);


	/*
	Following methods are used to get intermediate results after encryption/decryption
	*/
	vector<T> getEvVectorized(){ return this->Ev_vectorized; }
	vector<T> getEvNoisy(){ return this->Ev_noisy; }
	vector<T> getEvScaled(){ return this->Ev_scaled; }
	vector<T> getEvTangled(){ return this->Ev_tangled; }
	vector<T> getEvAmbiguous(){ return this->Ev_ambiguous; }

	vector<T> getEbVectorized(){ return this->Eb_vectorized; }
	vector<T> getEbNoisy(){ return this->Eb_noisy; }
	vector<T> getEbScaled(){ return this->Eb_scaled; }
	vector<T> getEbTangled(){ return this->Eb_tangled; }
	vector<T> getEbAmbiguous(){ return this->Eb_ambiguous; }

	//Checks if comparison and encryption-decryption is correct for selected values/
	bool validate(vector<T> Ev, vector<T> Eb, int val, int bnd);
	void print_stages_v();
	void print_stages_b();
	void print_key();

	

	vector<T> encrypt_v(T v);
	T decrypt_v(vector<T> Ev);
	vector<T> encrypt_b(T b);
	T decrypt_b(vector<T> Eb);


	T normalize_v(T v);
	vector<T> vectorize_v(T v);
	vector<T> add_noise_v(vector<T> Ev);
	vector<T> scale_v(vector<T> Ev);
	vector<T> tangle_v(vector<T> Ev);
	vector<T> add_ambiguity_v(vector<T> Ev);
	vector<T> resolve_ambiguity_v(vector<T> Ev);
	vector<T> untangle_v(vector<T> Ev);
	vector<T> remove_noise_v(vector<T> Ev);
	T scalarize_v(vector<T> Ev);
	T denormalize_v(T v);

	T normalize_b(T b);
	vector<T> vectorize_b(T b);
	vector<T> add_noise_b(vector<T> Eb);
	vector<T> scale_b(vector<T> Eb);
	vector<T> tangle_b(vector<T> Eb);
	vector<T> untangle_b(vector<T> Eb);
	vector<T> remove_noise_b(vector<T> Eb);
	T scalarize_b(vector<T> Eb);
	T denormalize_b(T b);

	void prepare_key() { initVectorU(); initMatrix(); key_prepared = true; };

private:
	//Initialize secret U vector
	void initVectorU();
	//Initialize secret M matrix
	void initMatrix();
	static vector<T> generateOrthogonalUnit(vector<T> v);

	bool key_prepared = false;

	const long u_seed, m_seed;

	vector<T> U;
	vector<vector<T> > M, Mt, Minv, Mtinv, I;

	vector<T> Ev_vectorized, Ev_scaled, Ev_noisy, Ev_tangled, Ev_ambiguous;
	vector<T> Eb_vectorized, Eb_scaled, Eb_noisy, Eb_tangled, Eb_ambiguous;

};



//DEFINITIONS

/*
* prepare_key - whether generate secret key or it will be provided manually later
* u_seed - seed for U vector generation
* m_seed - seed for M matrix generation
*/
template<typename T>
LAbESEncryptor<T>::LAbESEncryptor(const int noise_size, const int first_pos, const int second_pos, const bool ambig_end, const bool prepare_key, const long u_seed, const long m_seed): u_seed(u_seed), m_seed(m_seed) 
{
	if (noise_size < 2)
		throw std::invalid_argument("Wrong noise size");
	if (first_pos > second_pos || second_pos >= noise_size + 2)
		throw std::invalid_argument("Wrong first_pos or second_pos");

	this->m_noise_size = noise_size;
	this->m_total_size = noise_size + 2;
	this->m_first_pos = first_pos; 
	this->m_second_pos = second_pos; 
	this->m_ambig_end = ambig_end;
	if (prepare_key)
		this->prepare_key();
}

template<typename T>
LAbESEncryptor<T>::LAbESEncryptor(LAbESEncryptor<T> encr, const bool ambig_end) : LAbESEncryptor<T>(encr, encr.m_first_pos, encr.m_second_pos, ambig_end)
{
}

template<typename T>
LAbESEncryptor<T>::LAbESEncryptor(LAbESEncryptor<T> encr, const int first_pos, const int second_pos, const bool ambig_end)
{

	this->m_total_size = encr.m_total_size;
	this->m_noise_size = encr.m_noise_size; 
	this->m_first_pos = first_pos; 
	this->m_second_pos = second_pos;
	this->m_ambig_end = ambig_end;

	U = encr.U;
	M = encr.M;
	Mt = encr.Mt;
	Minv = encr.Minv;
	Mtinv = encr.Mtinv;
	I = encr.I;
	this->enable_scale(encr.is_scale_enabled());
	this->enable_noise(encr.is_noise_enabled());
	this->enable_tangle(encr.is_tangle_enabled());
	this->enable_ambiguity(encr.is_ambiguity_enabled());
}

template <typename T>
void LAbESEncryptor<T>::initVectorU()
{
	srand(this->u_seed);
	U = vector<T>(this->m_noise_size, 0.0);
	T length = 0.0;
	for (int count = 0; count < (int)U.size(); count++)
	{
		U[count] = LAbESRandom<T>::getValue(0);
		length +=  (U[count]*U[count]);
	}
	length = sqrt(length);
	for(int i=0;i<(int)U.size();i++) 
		U[i] = U[i]/length;
}


template<typename T>
void LAbESEncryptor<T>::initMatrix()
{
	//Naive way with diagonal dominance.
	//Particular good implementation is not discussed in the paper.

	M.resize(this->m_total_size,vector<T>(this->m_total_size, 0.0));
	Mt.resize(this->m_total_size,vector<T>(this->m_total_size, 0.0));
	Minv.resize(this->m_total_size,vector<T>(this->m_total_size, 0.0));
	I.resize(this->m_total_size,vector<T>(this->m_total_size, 0.0)); 
	Mtinv.resize(this->m_total_size,vector<T>(this->m_total_size, 0.0));
	int i, j;
	//I.diag() = 1;
	for(i = 0;i<this->m_total_size;i++){
		for(j = 0; j<this->m_total_size ; j++){
			if(i ==j){
				T one = T(1.0);
				I[i][j] = one;
			}else{
				T zero = T(0.0);
				I[i][j] = zero; 
			}
		}	
	}        

	T value = 0.0;

	cout << "M seed = " << this->m_seed << endl;
	srand (this->m_seed);

	vector< vector<T> > prod(this->m_total_size, vector<T>(this->m_total_size, 0.0));

	while (true) { 
		for (i = 0; i<this->m_total_size; i++){
			for (j = 0; j<this->m_total_size; j++){
				if(i==j) {M[i][j] = LAbESRandom<T>::getValue(0);}
				else {M[i][j] = LAbESRandom<T>::getValue(1);}
				Mt[j][i] = M[i][j];
			}
		}

		cout << "Computed M" << endl;

		cout.precision(PRECISION_DEC); 

		MatrixInverse(M, Minv);

		cout << "Computed Minv" << endl;

		MatrixMul(M, Minv, prod);

		cout << "Computed M Minv" << endl;

		print_matrix(prod);

		if (MatEqual(prod, I, COMPARISON_EPSILON_KEY))
			break;
		cerr << "initMatrix: bad M, trying again..." << endl;
	}

	cout << "M" << endl;


	for (i = 0; i<this->m_total_size; i++){
		for (j = 0; j<this->m_total_size; j++){
			Mtinv[j][i] = Minv[i][j];
		}
	}	

	cout << "Generated M" << endl;
}

template<typename T>
vector<T> LAbESEncryptor<T>::generateOrthogonalUnit(vector<T> v)
{
	//Naive way with last component always negative. 
	//Particular good implementation is not discussed in the paper.

	T totalValue = 0.0;
	vector<T> ort(v.size(), 0.0);
	T length = 0.0;
	for (int count = 0; count < (int)ort.size() - 1; count++)
	{
		ort[count] = LAbESRandom<T>::getValue(0);
		totalValue = totalValue + (v[count] * ort[count]);
		length += (ort[count]*ort[count]);
	}
	ort[ort.size() - 1] = -totalValue/v[ort.size() - 1];

	length += (ort[ort.size() - 1] * ort[ort.size() - 1]);
	length = sqrt(length);
	for(int i=0;i<(int)ort.size();i++)
		ort[i] = ort[i]/length;

	return ort;
}



// ---VALUES---
template<typename T>
vector<T> LAbESEncryptor<T>::encrypt_v(T v)
{
	if (!key_prepared)
		std::runtime_error("Secret key was not prepared!");

	return AbstractLAbESEncryptor<T>::encrypt_v(v);
}
template<typename T>
T LAbESEncryptor<T>::decrypt_v(vector<T> Ev)
{ 
	if (!key_prepared)
		std::runtime_error("Secret key was not prepared!");

	return AbstractLAbESEncryptor<T>::decrypt_v(Ev);
}



template<typename T>
T LAbESEncryptor<T>::normalize_v(T v)
{
	return v / INT_MAX;
}


template<typename T>
vector<T> LAbESEncryptor<T>::vectorize_v(T v)
{
	Ev_vectorized = vector<T>(this->m_total_size, 0.0 );
	Ev_vectorized[this->m_first_pos] = v;
	Ev_vectorized[this->m_second_pos] = -1;
	return Ev_vectorized;
}


template<typename T>
vector<T> LAbESEncryptor<T>::add_noise_v(vector<T> Ev)
{
	assert(Ev.size() == this->m_total_size);

	vector<T> nv = generateOrthogonalUnit(U);

	nv = VectorMulScalar(nv,LAbESRandom<T>::getValue(0)); 

	Ev_noisy = Ev;
	for (int count = 0; count<(int)Ev_noisy.size(); count++)
	{
		if (count < this->m_first_pos) 
			Ev_noisy[count] = nv[count];
		else
			if (count > this->m_first_pos && count < this->m_second_pos)
				Ev_noisy[count] = nv[count - 1];
			else
				if (count > this->m_second_pos)
					Ev_noisy[count] = nv[count - 2];
	}

	return Ev_noisy;
}

template<typename T>
vector<T> LAbESEncryptor<T>::scale_v(vector<T> Ev)
{       
	assert(Ev.size() == this->m_total_size);

	T q = Ev[this->m_first_pos]; 
	int x = (int) abs(tan(q));

	T scalar = x % 100;
	assert(scalar >= 0);
	if (scalar == 0 || scalar == 1) scalar = 2;

	Ev_scaled = VectorMulScalar(Ev,scalar);
	return Ev;
}


template<typename T>
vector<T> LAbESEncryptor<T>::tangle_v(vector<T> Ev)
{       
	assert(Ev.size() == this->m_total_size);

	Ev_tangled.resize(Ev.size()); 
	MatrixMulVector(Minv,Ev,Ev_tangled);
	return Ev_tangled;
}

template<typename T>
vector<T> LAbESEncryptor<T>::add_ambiguity_v(vector<T> Ev)
{
	assert(Ev.size() == this->m_total_size);

	vector<T> MtinvWu(Ev.size(), 0.0);
	for (int count = 0; count<(int)Ev.size(); count++)
	{
		if (count < this->m_first_pos)
			MtinvWu[count] = U[count];
		if (count > this->m_first_pos && count < this->m_second_pos)
			MtinvWu[count] = U[count - 1];
		if (count > this->m_second_pos)
			MtinvWu[count] = U[count - 2];
	}

	vector<T> Wu(Ev.size(), 0.0);

	MatrixMulVector(Mt, MtinvWu, Wu);

	Ev_ambiguous.resize(Ev.size() + 1);


	T q;
	if (this->m_ambig_end)
	{

		T denom = Wu[Ev.size() - 1];
		T nom = 0.0;

		cout << "denom " << denom << endl;

		assert(denom != 0);

		rotate(Wu.begin(),Wu.begin()+Wu.size()-1,Wu.end()); 

		for (int i = 0; i < (int)Ev.size(); i++)
			nom += Wu[i] * Ev[i];

		cout << "nom = " << nom << endl;

		cout << "Ev[0] = " << Ev[0] << endl;

		q = Ev[0] - nom / denom;

		for (int i = 0; i < (int)Ev.size(); i++)
			Ev_ambiguous[i] = Ev[i];
		Ev_ambiguous[Ev_ambiguous.size() - 1] = q;
	}
	else
	{
		T denom = Wu[0];
		T nom = 0.0 ;

		assert(denom != 0);
		rotate(Wu.begin(),Wu.begin()+1,Wu.end());
		for (int i = 0; i < (int)Ev.size(); i++)
			nom += Wu[i] * Ev[i];
		q = Ev[Ev.size() - 1] - nom / denom;

		for (int i = 0; i < (int)Ev.size(); i++)
			Ev_ambiguous[i + 1] = Ev[i];
		Ev_ambiguous[0] = q;
	}
	return Ev_ambiguous;
}

template<typename T>
vector<T> LAbESEncryptor<T>::resolve_ambiguity_v(vector<T> Ev)
{
	assert(Ev.size() == this->m_total_size + 1);
	vector<T> Ev_tangled(Ev.size() - 1, 0.0);
	if (this->m_ambig_end)
		for (int i = 0; i < (int)Ev_tangled.size(); i++)
			Ev_tangled[i] = Ev[i];
	else
		for (int i = 0; i < (int)Ev_tangled.size(); i++)
			Ev_tangled[i] = Ev[i + 1];
	return Ev_tangled;
}

template<typename T>
vector<T> LAbESEncryptor<T>::untangle_v(vector<T> Ev)
{
	assert(Ev.size() == this->m_total_size);
	Ev_noisy.resize(Ev.size());
	MatrixMulVector(M,Ev,Ev_noisy);
	return Ev_noisy;
}

template<typename T>
vector<T> LAbESEncryptor<T>::remove_noise_v(vector<T> Ev)
{
	assert(Ev.size() == this->m_total_size);
	Ev_scaled = Ev;
	for (int count = 0; count < (int)Ev.size(); count++) 
		if (count != this->m_first_pos && count != this->m_second_pos) Ev[count] = 0;
	return Ev_scaled;
}

template<typename T>
T LAbESEncryptor<T>::scalarize_v(vector<T> Ev)
{
	assert(Ev.size() == this->m_total_size);
	T v = -Ev[this->m_first_pos] / Ev[this->m_second_pos];
	return v;
}

template<typename T>
T LAbESEncryptor<T>::denormalize_v(T v)
{
	return v * INT_MAX;
}

//---QUERIES---
template<typename T>
vector<T> LAbESEncryptor<T>::encrypt_b(T b)
{
	if (!key_prepared)
		std::runtime_error("Secret key was not prepared!");

	return 	AbstractLAbESEncryptor<T>::encrypt_b(b);
}

template<typename T>
T LAbESEncryptor<T>::decrypt_b(vector<T> Eb)
{
	if (!key_prepared)
		std::runtime_error("Secret key was not prepared!");

	return AbstractLAbESEncryptor<T>::decrypt_b(Eb);
}

template<typename T>
T LAbESEncryptor<T>::normalize_b(T b)
{
	return b / INT_MAX;
}

template<typename T>
vector<T> LAbESEncryptor<T>::vectorize_b(T b)
{
	Eb_vectorized = vector<T> (this->m_total_size, 0.0);
	Eb_vectorized[this->m_first_pos] = 1;
	Eb_vectorized[this->m_second_pos] = b;
	return Eb_vectorized;
}

template<typename T>
vector<T> LAbESEncryptor<T>::add_noise_b(vector<T> Eb)
{
	assert(Eb.size() == this->m_total_size);
	T value = LAbESRandom<T>::getValue(0);
	Eb_noisy = Eb;
	for (int count = 0; count < (int)Eb_noisy.size(); count++)
	{
		if (count < this->m_first_pos) Eb_noisy[count] = U[count] * value;
		if (count > this->m_first_pos && count< this->m_second_pos) Eb_noisy[count] = U[count - 1] * value;
		if (count > this->m_second_pos) Eb_noisy[count] = U[count - 2] * value;
	}
	return Eb_noisy;
}

template<typename T>
vector<T> LAbESEncryptor<T>::scale_b(vector<T> Eb)
{
	assert(Eb.size() == this->m_total_size);

	T q = Eb[this->m_second_pos]; 
	int x = (int) abs(tan(q));

	T scalar = x % 100;
	assert(scalar >= 0);
	if (scalar == 0 || scalar == 1) scalar = 2;

	Eb_scaled = VectorMulScalar(Eb,scalar);

	return Eb_scaled;
}


template<typename T>
vector<T> LAbESEncryptor<T>::tangle_b(vector<T> Eb)
{
	assert(Eb.size() == this->m_total_size);
	Eb_tangled.resize(Eb.size());
	MatrixMulVector(Mt,Eb,Eb_tangled); 
	return Eb_tangled;
}

template<typename T>
vector<T> LAbESEncryptor<T>::untangle_b(vector<T> Eb)
{
	assert(Eb.size() == this->m_total_size);
	MatrixMulVector(Mtinv, Eb, Eb_noisy);
	return Eb_noisy;
}

template<typename T>
vector<T> LAbESEncryptor<T>::remove_noise_b(vector<T> Eb)
{
	assert(Eb.size() == this->m_total_size);
	Eb_scaled = Eb;
	for (int count = 0; count < (int)Eb.size(); count++) if (count != this->m_first_pos && count != this->m_second_pos) Eb[count] = 0;
	return Eb_scaled;
}

template<typename T>
T LAbESEncryptor<T>::scalarize_b(vector<T> Eb)
{
	assert(Eb.size() == this->m_total_size);
	T b = Eb[this->m_second_pos] / Eb[this->m_first_pos];
	return b;
}

template<typename T>
T LAbESEncryptor<T>::denormalize_b(T b)
{
	return b * INT_MAX;
}

template<typename T>
vector<T> LAbESEncryptor<T>::encrypt_int_v(long v)
{
	T v_t = v;
	vector<T> Ev = this->encrypt_v(v_t);
	return Ev;
}

template<typename T>
long LAbESEncryptor<T>::decrypt_int_v(vector<T> Ev)
{
	T v_t = this->decrypt_v(Ev);
	return to_integer(v_t);
}

template<typename T>
vector<T> LAbESEncryptor<T>::encrypt_int_b(long b)
{
	T b_t = b;
	vector<T> Eb = this->encrypt_b(b_t);
	return Eb;

}

template<typename T>
long LAbESEncryptor<T>::decrypt_int_b(vector<T> Eb)
{
	T b_t = this->decrypt_b(Eb);
	return to_integer(b_t);

}

template<typename T>
void LAbESEncryptor<T>::print_key() {
	cout << "U" << endl;
	cout << to_string_wprec(U) << endl;
	cout<<"M"<<endl;
	print_matrix<T>(M);
	cout<<"Mt"<<endl;
	print_matrix<T>(Mt);
	cout<<"Minv"<<endl;
	print_matrix<T>(Minv);
	cout<<"Mtinv"<<endl;
	print_matrix<T>(Mtinv);
}

template<typename T>
void LAbESEncryptor<T>::print_stages_v() {
	cout << "Ev_vectorized: " << to_string_wprec(Ev_vectorized) << endl;
	cout << "Ev_noisy: " << to_string_wprec(Ev_noisy) << endl;
	cout << "Ev_scaled: " << to_string_wprec(Ev_scaled) << endl;
	cout << "Ev_tangled: " << to_string_wprec(Ev_tangled) << endl;
	cout << "Ev_ambiguous: " << to_string_wprec(Ev_ambiguous) << endl;
}

template<typename T>
void LAbESEncryptor<T>::print_stages_b() {
	cout << "Eb_vectorized: " << to_string_wprec(Eb_vectorized) << endl;
	cout << "Eb_noisy: " << to_string_wprec(Eb_noisy) << endl;
	cout << "Eb_scaled: " << to_string_wprec(Eb_scaled) << endl;
	cout << "Eb_tangled: " << to_string_wprec(Eb_tangled) << endl;
	cout << "Eb_ambiguous: " << to_string_wprec(Eb_ambiguous) << endl;
}


template<typename T>
bool LAbESEncryptor<T>::validate(vector<T> Ev, vector<T> Eb, int val, int bnd) {
	assert(Ev.size() == Eb.size());
	assert(Ev.size() == this->m_total_size);

	typename vector<T>::const_iterator first;
	typename vector<T>::const_iterator last;

	vector<T> Ev_untangled = untangle_v(Ev);
	vector<T> Eb_untangled = untangle_b(Eb);
	first = Ev_untangled.begin();
	last = Ev_untangled.begin() + 2;
	vector<T> v(first, last);
	first = Ev_untangled.begin()+2;
	last = Ev_untangled.end();
	vector<T> nv(first, last);

	first = Eb_untangled.begin();
	last = Eb_untangled.begin() + 2;


	vector<T> b(first, last);
	first = Eb_untangled.begin()+2;
	last = Eb_untangled.end();
	vector<T> nb(first, last);

	int c1 = compare(val, bnd), c2 = compare(v, b), c3 = compare(Ev, Eb);

	bool same_sign = (c1 == c2) && (c2 == c3) ;
	bool noise_orthogonal = (compare(nv, nb) == 0);
	return same_sign && noise_orthogonal;
}




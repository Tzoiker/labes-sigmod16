#include "io.h"
using namespace std;

GzWriter::GzWriter(const char *const fn){
	f = gzopen(fn,"wb");
	if (!f){
		fprintf(stderr, "gzopen of '%s' failed:.\n", fn);
		exit(EXIT_FAILURE);
	}
}

GzWriter::~GzWriter(){ gzclose(f); }

void GzWriter::printf(const char *fmt, ...){
	static char s[10000];
	va_list argptr;
	va_start(argptr,fmt);
	int ns = vsprintf(s, fmt, argptr);
	va_end(argptr);

	assert(ns > 0);
	int w = gzwrite(f,s,ns);
	if (w == 0){
		int err_no = 0;
		fprintf(stderr, "Error during compression: %s", gzerror(f, &err_no));
		gzclose(f);
		exit(err_no);
	}
}

int count_lines_in_file(string path) {
	ifstream fs( path );
	int ret = -1;
	if (fs.is_open()) {
		int n = 0;
		string line;
		while ( getline (fs, line) ) n++;
		ret = n;
		fs.close();
	}
	return ret;
}
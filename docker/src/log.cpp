#include "log.h"
using namespace std;

fstream log_file;

void prepare_log(const string path) {
        log_file.open(path, fstream::out);
        if (log_file.is_open()) {
                cout << "prepare_log: log file was opened: " << path << endl;
        } else {
                cerr << "prepare_log: log file failed to open: " << path << endl;
                exit(-1);
        }
}

void log_line(const string s) {
        log_file << s << endl;
}



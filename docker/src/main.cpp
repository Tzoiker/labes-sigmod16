#include "stdafx.h"
#include "tester.h"
using namespace std;

//Alias for LAbESTester class with GMP multi-precision mpf_class as a storing format.
typedef LAbESTester<mpf_class> MPF_Tester;

/*
* Enty point for test running.
*/
int main(int argc, char** argv) {
	if (argc < 9){
		fprintf(stderr,"usage: ./main 1-[N] 2-[l] 3-[workload] 4-[min val] 5-[max val] 6-[selectivity] 7-[num of queries] 8-[plain or enc or ambig]\n");
		exit(1);
	}
	
	int N = atoi(argv[1]);
	int l = atoi(argv[2]);
	string workload(argv[3]);
	int mn = atoi(argv[4]);
	int mx = atoi(argv[5]);
	double selectivity = stod(argv[6]);
	unsigned int q_num = atoi(argv[7]);

	//Set default precision for mpf_class
	mpf_set_default_prec(PRECISION_BITS); 

	EncryptionMode mode;
	if (!strcmp(argv[8], "plain")) mode = EncryptionMode::Plain;
	else if (!strcmp(argv[8], "enc")) mode = EncryptionMode::Enc;
	else if (!strcmp(argv[8], "ambig")) mode = EncryptionMode::Ambig;
	else throw invalid_argument(string("main: incorrect mode: ") + argv[8]);

	MPF_Tester tester(N, l, workload, selectivity, q_num, mn, mx);
	bool res;
	switch(mode) {
		case EncryptionMode::Plain:
			res = tester.test_plain();
			break;
		case EncryptionMode::Enc:
			res = tester.test_enc();
			break;
		case EncryptionMode::Ambig:
			res = tester.test_ambig();
			break;
	}

	return 0;
}

#pragma once

#include "stdafx.h"
#include <fstream>

/*
* Logger to text file.
*/

void prepare_log(const string path = "log.txt");
void log_line(const string s);

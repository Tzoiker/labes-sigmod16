#pragma once

#include "misc.h"
using namespace std;

void tokenize(const string& str, vector<string>& tokens, const string& delimiters)
{
	// Skip delimiters at beginning.
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	// Find first "non-delimiter".
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	while (string::npos != pos || string::npos != lastPos)
	{
		// Found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));
		// Skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);
		// Find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}
}

void vector_from_string(const string s, vector<mpf_class> &v) {
	vector<string> tokens;         
	tokenize(s, tokens);
	v.reserve(tokens.size());
	for(int i = 0; i < (int)tokens.size(); i++)
		v.push_back(mpf_class(tokens[i]));
}

void vector_from_string(const string s, vector<double> &v) {
	vector<string> tokens;         
	tokenize(s, tokens);
	v.reserve(tokens.size());
	for(int i = 0; i < (int)tokens.size(); i++)
		v.push_back(stod(tokens[i]));
}

void scalar_from_string(string s, mpf_class &v) {
	v = mpf_class(s, v.get_prec());
}

void scalar_from_string(string s, double &v) {
	v = stod(s);
}

string to_string_wprec(const mpf_class &mpf, const int prec) {
	cout.precision(1);

	long s = mpf.get_si();
	mp_exp_t exp;
	string str = mpf.get_str(exp, 10, 100);
	int e = exp;//-(int)significand.size() ;
	char ch = str[0];
	char m = '-';
	int startidx = 0;
	if(ch == m){
		startidx=1;                 
	}
	if(e != 0){
		if(e == 1 && s != 0){
			if(startidx == 1)
				str = "-" + str.substr(1,1)+ "." + str.substr(2,str.length()-1); 
			else if(e == 1 && str.length() == 1)
				str = str + ".0"; 
			else                
				str = str.substr(0,1)+ "." + str.substr(1,str.length()-1);
		}
		else if(e > 0){
			if ((int)str.length() > e){  
				str = str.substr(0, exp + startidx) + "." + str.substr(exp + startidx,str.length()-1);
			}else{
				int len = str.length(); 
				for(int i = len;i<=exp;i++){

					if(i==exp){
						str = str+".0";
					}else{
						str = str +"0";	
					} 
				}	
			} 
		}else{
			if(startidx == 1){
				str = str.substr(1,str.length()-1);	
			}
			for(int i=0;i<abs(e);i++){
				str = "0" + str;           	
			}
			if(startidx == 1){
				str  = "-0." + str;
			}else{
				str = "0." + str;
			}
		}       

	}else{
		if(startidx == 1){
			str = "-0." + str.substr(1,str.length()-1);	
		}else{
			str = "0." + str;
		}         
	}
	return str;
}

string to_string_wprec(const double d, const int prec) {
	ostringstream strs;
	strs << setprecision(prec) << d;
	return strs.str();
}

string to_string_wprec(const int i, const int prec) {
	return to_string(i);
}

string to_string_wprec(const vector<mpf_class> &v, const int prec) {
	string vectorString;
	for (int i = 0; i < (int)v.size(); i++) {
		vectorString += to_string_wprec(v[i], prec);
		vectorString += "_";
	}
	return vectorString;
}

string to_string_wprec(const vector<double> &v, int prec) {
	string vectorString;
	for (int i = 0; i < (int)v.size(); i++) {
		vectorString += to_string_wprec(v[i], prec);
		vectorString += "_";
	}
	return vectorString;
}

double tan(mpf_class v) {
	return tan(v.get_d());
}


long to_integer(mpf_class &v) {
	return round(v).get_si();
}
long to_integer(double &v) {
	return (long) round(v);
}

mpf_class round(mpf_class &v) {
	mpf_class diff = v - trunc(v);

	if (diff >= 0)
		if (diff < 0.5)
			return floor(v);
		else
			return ceil(v);
	else
		if (abs(diff) < 0.5)
			return ceil(v);
		else
			return floor(v);
}

template void print_matrix(vector<vector<double>> &);
template void print_matrix(vector<vector<mpf_class>> &);
#pragma once

#include "stdafx.h"

struct Timer {
	struct timeval tv0, tv1;
	double total;
	void start(){ gettimeofday(&tv0,0); }
	void stop(){ gettimeofday(&tv1,0); total += tv1.tv_sec - tv0.tv_sec + (tv1.tv_usec - tv0.tv_usec)*1e-6; }
	void clear(){ total = 0; }
	double elapsed() const { return total; }
};

double timing();
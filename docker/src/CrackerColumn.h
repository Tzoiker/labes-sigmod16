#pragma once

#include "Crackers.h"
#include "stdafx.h"

#define DEFAULT_CLUSTER_SIZE 4096

typedef struct {
	int min_size;
	int max_size;
	int avg_size;
} CrackersInfo;

typedef struct {
	int pos_left;
	int pos_right;
	int touched;
	double search_t;
	double crack_t;
	double insert_t;
} CrackersResult;

/*
* Represents a cracker column, storing data of type T, can be double, mpf_class, etc.
* Consists of an array of data _arr and an AVL tree _AVLTree, which represents the cracks of the column.
* The cracking itself is performed with a helper class Crackers.
*/
template<typename T>
class CrackerColumn {
private:

	bool _is_random_crack_enabled = false;
	unsigned int _random_crack_period = 1;
	unsigned int _queries_count = 0;
	Random random;
	void naive_random_crack();

	bool _is_crack_size_lower_bouned = false;
	unsigned int _crack_size_lower_bound = DEFAULT_CLUSTER_SIZE / sizeof(double); //Must be not double but number of bytes occupied by T type. Leave as some approximation.

	vector<T> _arr;
	AVLTree<T> _AVLTree;

	Timer _t_crack, _t_tree_search, _t_tree_insert;

	CrackersResult crack(const T &a_key, const T &a_val, const T &b_key, const T &b_val);
	
public:
	CrackerColumn(vector<T> &arr): _arr(arr) {};
	~CrackerColumn() {clear();};


	void clear() { _arr.clear(); _AVLTree.clear();}

	void enable_random_crack(bool enable) { _is_random_crack_enabled = enable; }
	void set_random_crack_period(unsigned int period) { if (period == 0) throw std::invalid_argument("Period must be greater than 0."); _random_crack_period = period; }

	void enable_crack_size_lower_bound(bool enable) { _is_crack_size_lower_bouned = true; }
	void set_crack_size_lower_bound(unsigned int size){ _crack_size_lower_bound = size; }

	double get_crack_time() const { return _t_crack.elapsed(); }
	double get_tree_search_time() const { return _t_tree_search.elapsed(); }
	double get_tree_insert_time() const { return _t_tree_insert.elapsed(); }
	void reset_time() { _t_crack.clear(); _t_tree_search.clear(); _t_tree_insert.clear(); }

	unsigned int get_number_of_cracks() const { _AVLTree.getSize(); }
	void print_cracks() const { cout << "Cracks: ";_AVLTree.print_cracks(); }
	void print_arr() const {
		for (auto it = _arr.begin(); it != _arr.end(); ++it)
			cout << *it << "   ";
		cout << endl;
	}

	vector<T> get_values(int p1, int p2) { 
		if (p2 < p1 || p2 < 0)
			throw invalid_argument("p2 must be >= p1 and >= 0!");
		if (p1 < 0) p1 = 0;
		if (p2 >= _arr.size()) p2 = _arr.size() - 1;
		return vector<T>(_arr.begin() + p1, _arr.begin() + p2 + 1); 
	}

	const vector<T>& get_immutable_arr_ref() const { return _arr; }

	//Evaluates range query and cracking
	CrackersResult view_query(const T &a_key, const T &a_val, const T &b_key, const T &b_val);
	CrackersInfo get_crackers_info() const;
};


template<typename T>
CrackersResult CrackerColumn<T>::view_query(const T &a_key, const T &a_val, const T &b_key, const T &b_val){
	return crack(a_key, a_val, b_key, b_val); 
}


//key - value compared with key in the tree
//val - value stored as key in the tree
//For simple data types it is the same
template<typename T>
CrackersResult CrackerColumn<T>::crack(const T &a_key, const T &a_val, const T &b_key, const T &b_val){

	int n_touched = 0; _t_tree_search.clear(); _t_crack.clear(); _t_tree_insert.clear();

	int N = _arr.size(), i1 = -1, i2 = -1;

	_t_tree_search.start();
	int L1, R1; Crackers<T>::find_piece(_AVLTree, N, a_key, L1, R1);
	int L2, R2; Crackers<T>::find_piece(_AVLTree, N, b_key, L2, R2);
	_t_tree_search.stop();

	log_line(to_string(L1) + "    " + to_string(R1) + "    " + to_string(L2) + "    " + to_string(R2));

	assert (L1 <= L2 && R1 <= R2);

	bool insert_left = false;
	bool insert_right = false;

	_t_crack.start();
	if (L1==L2){            // a and b is on the same piece
		assert(R1 == R2);
		Crackers<T>::split_ab(_arr, L1, R1, a_key, b_key, i1, i2);  // 3-split in one scan 
		n_touched = R1 - L1;
		if (!_is_crack_size_lower_bouned || (R1 - L1) > _crack_size_lower_bound) {	
			insert_left = true;
			insert_right = true;
		}
	} else {                // a and b is on different piece
		if (!_is_crack_size_lower_bouned || (R1 - L1) > _crack_size_lower_bound)
			insert_left = true;
		i1 = Crackers<T>::partition(_arr, a_key, L1, R1);
		n_touched += R1 - L1;
		
		if (!_is_crack_size_lower_bouned || (R2 - L2) > _crack_size_lower_bound)
			insert_right = true;
		i2 = Crackers<T>::partition(_arr, b_key, L2, R2);
		n_touched += R2 - L2;
		
	}
	_t_crack.stop();


	assert( ((i1 >= 0 && i1 <= N) || !insert_left) && ((i2 >= 0 && i2 <= N) || !insert_right)  );

	_t_tree_insert.start();
	if (insert_left)
		Crackers<T>::add_crack(_AVLTree, N, a_val, a_key, i1);
	if (insert_right)
		Crackers<T>::add_crack(_AVLTree, N, b_val, b_key, i2);
	_t_tree_insert.stop();

	return CrackersResult{i1, i2, n_touched, _t_tree_search.elapsed(), _t_crack.elapsed(), _t_tree_insert.elapsed()};    // return number of qualified tuples
}

template<typename T>
CrackersInfo CrackerColumn<T>::get_crackers_info() const {	
	vector<int> positions = _AVLTree.list_cracks();

	int min = INT_MAX;
	int max = INT_MIN;
	int num = positions.size() + 1;

	for (int i = 0; i < positions.size(); i++) {
		int size;
		if (i == 0) {
			size = positions[i] + 1;
			assert (positions[i] != 0); 
		} else if (i == positions.size() - 1) {
			size = _arr.size() - positions[i];
			assert(positions[i] != _arr.size() - 1); 
		} else {
			size = positions[i] - positions[i-1] + 1;
		}
		if (size < min) min = size;
		if (size > max) max = size;
	}

	return CrackersInfo{ min, max, _arr.size() / num };
}

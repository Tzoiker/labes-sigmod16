#pragma once

#include "stdafx.h"

using namespace std;

/*
* Abstract numeric data encryptor class
*/
template <typename T>
class AbstractEncryptor
{
public:
	virtual vector<T> encrypt_v(T v) = 0;
	virtual T decrypt_v(vector<T> Ev) = 0;

	virtual vector<T> encrypt_b(T b) = 0;
	virtual T decrypt_b(vector<T> Eb) = 0; 
};

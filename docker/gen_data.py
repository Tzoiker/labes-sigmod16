import subprocess
from subprocess import call
import sys

assert(len(sys.argv) == 2)
scale=float(sys.argv[1])

M1 = int(10**6*scale)
M2 = M1

mn = 0
mx = int((2**31-1)*scale)

def prepare_data(N, l):
    comm = ['make', 'data/generate', 'N=' + str(N), 'L=' + str(l), 'MIN=' + str(mn), 'MAX=' + str(mx)]
    call(comm, stderr=subprocess.STDOUT)

def main():

    Ns = [k*M1 for k in [1,2,4,8,16,32]]
    l = 4
    for N in Ns:
        prepare_data(N, l)

    N = M2
    ls = [4,8,16,32,64]
    for l in ls:
        prepare_data(N, l)

    
if __name__ == "__main__":
    main()


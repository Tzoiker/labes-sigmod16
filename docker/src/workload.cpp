#include "workload.h"

using namespace std;

Workload::Workload(const char *workload, const int selectivity, const int min, const int max): S(selectivity), _min(min), _max(max) {
	r = Random(29284);
	const char *names[2] = {  // workload names
		"Random",        // 0
		"RandomIncSelectivity" //1
	};

	for (I=W=0; W<2 && strcmp(workload, names[W]); W++);

	if (W == 2){
		fprintf(stderr,"Workload \"%s\" is not found!\n",workload);
		exit(1);
	}
}

bool Workload::query(int &na, int &nb){
	switch (W){
	case 0 : if (!random_w()) return false; break;
	case 1: if (!random_increase_selectivity()) return false; break;
	default : assert(0);
	}
	na = a; nb = b; I++;
	return true;
}


// a and b is selected uniformly at random and a < b
bool Workload::random_w(){
	if (S == 0){ 
		do {
			a = r.nextIntBounded(_min, _max);
			b = r.nextIntBounded(_min, _max);
		}while(a == b);
		assert(a >= _min && b >= _min && b <= _max && a <= _max);
		if (a > b) swap(a, b);
	} else {
		do {
			a = r.nextIntBounded(_min, _max - 1);
			b = max(a + 1, a + S);
		} while(b < a);
	}
	return true;
}

//Selectivity increased progressively every 200 queries starting from 0.1%
bool Workload::random_increase_selectivity() {
	if (I == 0)
		S = (int) ( 0.001 * (_max - _min) );
	else if ( I / 200 != (I - 1) / 200 )
		S = (int) ( 0.001 * pow(3, (int)(I / 200)) * (_max - _min) );
	assert(S != 0);
	return random_w();
}


import subprocess
from subprocess import call
import sys

assert(len(sys.argv) == 2)
scale=float(sys.argv[1])

M1 = int(10**6*scale)
M2 = M1
Q = int(50000*scale)

mn = 0
mx = int((2**31-1)*scale)
s = '1e-2'


def main():
    W = 'Random'
    qnum = Q

    Ns = [k*M1 for k in [1,2,4,8,16,32]]
    l = 4
    for N in Ns:
        for mode in ['plain', 'enc', 'ambig']:
            comm = ['./query.sh', str(N), str(l), W, str(mn), str(mx), str(s), str(qnum), mode]
            call(comm, stderr=subprocess.STDOUT)

    N = M2
    ls = [4,8,16,32,64]
    for l in ls:
        comm = ['./query.sh', str(N), str(l), W, str(mn), str(mx), str(s), str(qnum), 'enc']
        call(comm, stderr=subprocess.STDOUT)
    
    
    W = 'RandomIncSelectivity'
    N = M2
    l = 4
    qnum = 10**3
    for mode in ['plain', 'enc', 'ambig']:
        comm = ['./query.sh', str(N), str(l), W, str(mn), str(mx), str(s), str(qnum), mode]
        call(comm, stderr=subprocess.STDOUT)

    
if __name__ == "__main__":
    main()


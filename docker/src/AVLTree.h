#pragma once


#include <iostream>
#include <vector>
#include "LAbESEncryptor.h"
#include "stdafx.h"

using namespace std;
template<typename T>
class Node
{
private:
	T key;
	int pos;    //cracker Index Position
	int holes;    // the number of holes in front
	bool sorted;  // is this piece sorted
	int height;
	Node<T> * parent = 0;
	Node<T> * left_child = 0;
	Node<T> * right_child = 0;
public:
	int prev_pos() const { return pos - holes; }
	Node(T val, int p);
	T getkey() { return key; }
	int getPos(){ return pos; }
	int getHeight() { return height; }
	void setPos(int p){ pos = p; }  
	int updateHeight();
	unsigned int getSize();
	int getBalance();
	Node<T>* getParent() { return parent; }
	void removeParent(){ parent = (Node<T>*)0; }
	Node<T>* getLeftChild() { return left_child; }
	Node<T>* setLeftChild(Node<T>* new_left);
	Node<T>* getRightChild() { return right_child; };
	Node<T>* setRightChild(Node<T>* new_right);
	int findDepth();
};

template<typename T>
class AVLTree
{
private:
	void setRoot(Node<T>* n);
	Node<T> * root;
	unsigned int _size;

	vector<int> list_cracks(Node<T> * n, vector<int> &list) const {
		assert(n != 0);

		Node<T>* left = n->getLeftChild();
		Node<T>* right = n->getRightChild();

		if (left != 0)
			list_cracks(left, list);
		
		list.push_back(n->getPos());

		if (right != 0)
			list_cracks(right, list); 
	}

	static void clear(Node<T> * node) {
		if (node == 0)
			return;
		Node<T>* left = node->getLeftChild();
		Node<T>* right = node->getRightChild(); 
		Node<T>* parent = node->getParent();
		if (left != 0)
			clear(left);
		if (right != 0)
			clear(right);
		if (parent!= 0) {
			if (parent->getLeftChild() == node)
				parent->setLeftChild(0);
			else if (parent->getRightChild() == node)
				parent->setRightChild(0);
			else assert(false);
		}
		delete node;
	}

	void treeAsListOfStrings(Node<T> *n, vector<vector<string>> &res);

public:

	unsigned int getSize() { return _size; }

	Node<T>* getRoot(){ return root; }
	Node<T>* findNode(T val);
	
	void rotateLeft(Node<T> * n);

	Node<T>* findMin(Node<T>* p) // find a node with minimal key in a p tree 
	{			
		return p->getLeftChild()?findMin(p->getLeftChild()):p;
	}
	Node<T>* findMax(Node<T>* p) // find a node with minimal key in a p tree 
	{
		return p->getRightChild()?findMax(p->getRightChild()):p;
	}
	

	Node<T>* findNextLargest(Node<T>* currentNode,Node<T>* max);

	Node<T>* findPrevMinimum(Node<T>* currentNode,Node<T>* min);                     

	void rotateRight(Node<T> * n);

	// This function does balancing at the given node
	void balanceAtNode(Node<T>* n);

	AVLTree()
	{
		root = (Node<T>*)0;
		_size = 0;
	}

	AVLTree(T val,int pos)
	{
		root = new Node<T>(val,pos);
		_size = 1;
	}

	void clear() {
		clear(root);
		root = (Node<T>*)0;
		_size = 0;	
	}

	AVLTree<T> * findSubtree(T val);

	// Returns:
	// 		true: If addition is successful
	// 		false: If item already exists
	//
	bool insert(T val, T b, int pos);

	// Returns:
	// 		true: If removal is successful
	// 		false: If item is not found in the tree
	//
	bool remove(T val);
	int getHeight() { return root->getHeight(); }

	void printTree();
	void printTreeNice();

	void print(Node<T> *n);

	vector<vector<string>> treeAsListOfStrings() {
		vector<vector<string>> res;
		treeAsListOfStrings(root, res);
		assert(root != 0);
		int sz = res.size();

		bool dummy = true;
		for (auto it = res.at(sz-1).begin(); it != res.at(sz-1).end(); ++it)
			if (strcmp((*it).c_str(), "-") != 0) {
				dummy = false; break;
			}

		res.erase(res.begin() + (sz - 1));
		return res;
	};
	
	vector<int> list_cracks() const {
		vector<int> list;
		list_cracks(root, list);
		return list;
	}

	void print_cracks() const { 
		vector<int> list = list_cracks();
		for (auto it = list.begin(); it != list.end(); ++it)
			cout << *it << "    ";
		cout << endl; 
	}

};


//METHOD DEFINITIONS

template<typename T>
Node<T>::Node(T val, int p)
{
	key = val;
	pos = p;
	holes = 0;
	sorted = false; 
	height = 0;
	parent = (Node<T>*)0;
	left_child = (Node<T>*)0;
	right_child = (Node<T>*)0;
}

template<typename T>
int Node<T>::updateHeight()
{
	if(left_child != 0 && right_child != 0)
	{
		if(left_child->getHeight() > right_child->getHeight())
			height = left_child->getHeight() + 1;
		else
			height = right_child->getHeight() + 1;
	}
	else if(left_child != 0)
		height = left_child->getHeight() + 1;
	else if(right_child != 0)
		height = right_child->getHeight() + 1;
	else
		height = 0;
	return height;
}

template<typename T>
int Node<T>::getBalance()
{
	Node<T> * n = this;
	if(n->getLeftChild() != 0 && n->getRightChild() !=0)
		return n->getLeftChild()->getHeight() - n->getRightChild()->getHeight();
	else if(n->getLeftChild() != 0)
		return n->getLeftChild()->getHeight() + 1;
	else if(n->getRightChild() != 0)
		return (-1) * (n->getRightChild()->getHeight() + 1);
	else
		return 0;
}

template<typename T>
unsigned int Node<T>::getSize()
{
	Node<T> * n = this;
	unsigned int size = 0;
	Node<T> * lc = n->getLeftChild();
	Node<T> * rc = n->getRightChild();
	if (lc != 0)
		size += lc->getSize();
	if (rc != 0)
		size += rc->getSize();
	size += 1;
	return size;
}

template<typename T>
Node<T>* Node<T>::setLeftChild(Node<T>* new_left)
{
	if(new_left != 0) new_left->parent = this;
	left_child = new_left;
	updateHeight();
	return left_child;
}

template<typename T>
Node<T>* Node<T>::setRightChild(Node<T>* new_right)
{
	if(new_right != 0) new_right->parent = this;
	right_child = new_right;
	updateHeight();
	return right_child;
}

template<typename T>
int Node<T>::findDepth() {
	int depth = 0;
	Node<T>* n = this;
	while(n->parent != 0) {
		depth++;
		n = n->parent;
	}
	return depth;
}


//AVLTree
template<typename T>
void AVLTree<T>::setRoot(Node<T>* n)
{
	root = n;
	if(root != 0) root->removeParent();
	_size = root->getSize();
}


template<typename T>
Node<T>* AVLTree<T>::findNode(T val){
	Node<T>* temp,*prev;
	temp = root;
	prev = root; 
	while(temp != 0)
	{
		if(compare_bv(val, temp->getkey()) == 0) {
			return temp;
		}
		else if(compare_bv(val, temp->getkey()) < 0){
			prev = temp;
			temp = temp->getLeftChild();
		}
		else{
			prev = temp;
			temp = temp->getRightChild();
		}
	}
	return prev;
}

template<typename T>
void AVLTree<T>::rotateLeft(Node<T> * n)
{
	Node<T> * p = n->getParent();
	enum {left, right} side;
	if(p != 0)
	{
		if(p->getLeftChild() == n)
			side = left;
		else
			side = right;
	}
	Node<T> * temp = n->getRightChild();
	n->setRightChild(temp->getLeftChild());
	temp->setLeftChild(n);
	// Now attach the subtree to the parent (or root)
	if(p != 0)
	{
		if(side == left)
			p->setLeftChild(temp);
		if(side == right)
			p->setRightChild(temp);
	}
	else
	{
		setRoot(temp);
	}
}


template<typename T>
Node<T>* AVLTree<T>::findNextLargest(Node<T>* currentNode, Node<T>* max){
	if(currentNode == max)
		//return 0;
		return currentNode;
	if (currentNode == 0)
		return 0;
	// cout<<"Here we are:In find largest:"<<endl;
	if (currentNode->getRightChild() != 0)
		return findMin(currentNode->getRightChild());

	Node<T>* y = currentNode->getParent();
	Node<T>* x = currentNode;
	while (y != 0 && x == y->getRightChild())
	{
		x = y;
		y = y->getParent();
	}

	return y;
}

template<typename T>
Node<T>* AVLTree<T>::findPrevMinimum(Node<T>* currentNode, Node<T>* min){
	if(currentNode == min)
		return currentNode;
	if (currentNode == 0)
		return 0;
	//cout<<"In Finding Prev Minimum 1:"<<endl;
	if (currentNode->getLeftChild() != 0)
		return findMax(currentNode->getLeftChild());
	//cout<<"In Finding Prev Minimum 2:"<<endl;
	Node<T>* y = currentNode->getParent();
	Node<T>* x = currentNode;
	while (y != 0 && x == y->getLeftChild())
	{
		x = y;
		y = y->getParent();
	}
	//cout<<"In Finding Prev Minimum 3:"<<endl;
	return y;
}                        


template<typename T>
void AVLTree<T>::rotateRight(Node<T> * n)
{
	Node<T> * p = n->getParent();
	enum {left, right} side;
	if(p != 0)
	{
		if(p->getLeftChild() == n)
			side = left;
		else
			side = right;
	}
	Node<T> * temp = n->getLeftChild();
	n->setLeftChild(temp->getRightChild());
	temp->setRightChild(n);
	// Now attach the subtree to the parent (or root)
	if(p != 0)
	{
		if(side == left)
			p->setLeftChild(temp);
		if(side == right)
			p->setRightChild(temp);
	}
	else
	{
		setRoot(temp);
	}
}

// This function does balancing at the given node
template<typename T>
void AVLTree<T>::balanceAtNode(Node<T>* n)
{			
	int bal = n->getBalance();
	if(bal > 1)
	{
		if(n->getLeftChild()->getBalance() < 0)
			rotateLeft(n->getLeftChild());
		rotateRight(n);
	}
	else if(bal < -1)
	{
		if(n->getRightChild()->getBalance() > 0)
			rotateRight(n->getRightChild());
		rotateLeft(n);
	}
}

template<typename T>
AVLTree<T> * AVLTree<T>::findSubtree(T val)
{
	Node<T>* target;
	target = findNode(val);

	if(target != 0)
	{
		AVLTree<T>* subtree = new AVLTree<T>();
		subtree->setRoot(target);
		return subtree;
	}
	else
		return (AVLTree<T>*)0;
}

// Returns:
// 		true: If addition is successful
// 		false: If item already exists
//
template<typename T>
bool AVLTree<T>::insert(T val, T b, int pos)
{
	Node<T>* added_node;
	if(root == 0)
	{
		root = new Node<T>(val, pos);
		_size++;
		return true;
	}
	else
	{
		Node<T>* temp = root;

		while(true)
		{
			if(compare_vb(temp->getkey(), b) == 0)
			{
				return false;
			}
			else if(compare_vb(temp->getkey(), b) >= 0)
			{
				if((temp->getLeftChild()) == 0)
				{
					added_node = temp->setLeftChild(new Node<T>(val,pos));
					break;
				}
				else
				{
					temp = temp->getLeftChild();
				}

			}
			else if(compare_vb(temp->getkey(), b) < 0)
			{
				if((temp->getRightChild()) == 0)
				{
					added_node = temp->setRightChild(new Node<T>(val,pos));
					break;
				}
				else
				{
					temp = temp->getRightChild();
				}

			}
		}
		// The following code is for updating heights and balancing.
		temp = added_node;
		while(temp != 0)
		{
			temp->updateHeight();
			balanceAtNode(temp);
			temp = temp->getParent();
		}
	}
	_size++;
	return true;
}

// Returns:
// 		true: If removal is successful
// 		false: If item is not found in the tree
//
template<typename T>
bool AVLTree<T>::remove(T val)
{
	if(root == 0) return false;
	Node<T> *replacement, *replacement_parent, *temp_node;
	Node<T> * to_be_removed = findNode(val);			
	if(to_be_removed == 0) return false;

	Node<T> * p = to_be_removed->getParent();

	enum {left, right} side;
	if(p != 0)
	{
		if(p->getLeftChild() == to_be_removed) side = left;
		else side = right;
	}

	int bal = to_be_removed->getBalance();

	if(to_be_removed->getLeftChild() == 0 && to_be_removed->getRightChild() == 0)
	{
		if(p != 0)
		{
			if(side == left) p->setLeftChild((Node<T>*)0);
			else p->setRightChild((Node<T>*)0);

			delete to_be_removed;
			p->updateHeight();
			balanceAtNode(p);
		}
		else
		{
			setRoot((Node<T>*)0);
			delete to_be_removed;
		}

	}
	else if(to_be_removed->getRightChild() == 0)
	{
		if(p != 0)
		{
			if(side == left) p->setLeftChild(to_be_removed->getLeftChild());
			else p->setRightChild(to_be_removed->getLeftChild());

			delete to_be_removed;
			p->updateHeight();
			balanceAtNode(p);
		}
		else
		{
			setRoot(to_be_removed->getLeftChild());
			delete to_be_removed;
		}
	}

	else if(to_be_removed->getLeftChild() == 0)
	{
		if(p != 0)
		{
			if(side == left) p->setLeftChild(to_be_removed->getRightChild());
			else p->setRightChild(to_be_removed->getRightChild());

			delete to_be_removed;
			p->updateHeight();
			balanceAtNode(p);
		}
		else
		{
			setRoot(to_be_removed->getRightChild());
			delete to_be_removed;
		}
	}
	else
	{
		if(bal > 0)
		{
			if(to_be_removed->getLeftChild()->getRightChild() == 0)
			{
				replacement = to_be_removed->getLeftChild();
				replacement->setRightChild(to_be_removed->getRightChild());

				temp_node = replacement;


			}
			else
			{
				replacement = to_be_removed->getLeftChild()->getRightChild();
				while(replacement->getRightChild() != 0)
				{
					replacement = replacement->getRightChild();
				}
				replacement_parent = replacement->getParent();
				replacement_parent->setRightChild(replacement->getLeftChild());

				temp_node = replacement_parent;

				replacement->setLeftChild(to_be_removed->getLeftChild());
				replacement->setRightChild(to_be_removed->getRightChild());
			}
		}
		else
		{
			if(to_be_removed->getRightChild()->getLeftChild() == 0)
			{
				replacement = to_be_removed->getRightChild();
				replacement->setLeftChild(to_be_removed->getLeftChild());

				temp_node = replacement;


			}
			else
			{
				replacement = to_be_removed->getRightChild()->getLeftChild();
				while(replacement->getLeftChild() != 0)
				{
					replacement = replacement->getLeftChild();
				}
				replacement_parent = replacement->getParent();
				replacement_parent->setLeftChild(replacement->getRightChild());

				temp_node = replacement_parent;

				replacement->setLeftChild(to_be_removed->getLeftChild());
				replacement->setRightChild(to_be_removed->getRightChild());
			}
		}		
		if(p != 0)
		{
			if(side == left) p->setLeftChild(replacement);
			else p->setRightChild(replacement);

			delete to_be_removed;
		}
		else
		{
			setRoot(replacement);
			delete to_be_removed;
		}

		balanceAtNode(temp_node);
	}
	_size--;
	return true;
}


template<typename T>
void AVLTree<T>::printTree(){
	cout << "\nPrinting the tree...\n";
	cout << "Root Node: " << root->getPos()<< "\n\n";
	print(root);
}

template<typename T>
void AVLTree<T>::printTreeNice(){
	cout << "\nPrinting the tree nicely...\n";
	cout << endl;
	vector<vector<string>> res = treeAsListOfStrings();
	int spacing = 36;
	for (int i = 0; i < res.size(); i++) {
		vector<string> lvl = res.at(i);
		for (auto it_in = lvl.begin(); it_in != lvl.end(); ++it_in) {
			int new_spacing = spacing / (pow(2.0, i) + 1);
			cout << setw(new_spacing) << *it_in;
		}
		cout << endl;
	}
	cout << endl;
}

template<typename T>
void AVLTree<T>::print(Node<T> *n)
{
	if(n != 0)
	{
		cout<<"Node: " << n->getPos()<<"\n";
		if(n->getLeftChild() != 0)
		{
			cout<<"\t moving left\n";
			print(n->getLeftChild());
			cout<<"Returning to Node" << n->getPos() << " from its' left subtree\n";
		}
		else
		{
			cout<<"\t left subtree is empty\n";
		}
		cout<<"Node: " << n->getPos() <<"\n";
		if(n->getRightChild() != 0)
		{
			cout<<"\t moving right\n";
			print(n->getRightChild());
			cout<<"Returning to Node" << n->getPos() << " from its' right subtree\n";
		}
		else
		{
			cout<<"\t right subtree is empty\n";
		}
	}
}

template<typename T>
void AVLTree<T>::treeAsListOfStrings(Node<T> *n, vector<vector<string>> &res)
{
	assert(n != 0);

	Node<T> *left = n->getLeftChild();
	Node<T> *right = n->getRightChild();

	//cout << "B0" << n << endl;
	int depth = n->findDepth();
	assert(res.size() >= depth);

	T key = n->getkey();
	if (res.size() == depth) 
		res.push_back(vector<string>());

	if (left == 0) {
		if (res.size() - 1 < depth + 1) 
		 	res.push_back(vector<string>());
		res.at(depth + 1).push_back("-");
	}

	res.at(depth).push_back(to_string_wprec(key));


	

	if (left != 0)
		treeAsListOfStrings(left, res); 
	if (right != 0)
		treeAsListOfStrings(right, res);

	if (right == 0) {
		assert(res.size() >= depth + 2);
		if (res.size() - 1 < depth + 1) 
			res.push_back(vector<string>());
		res.at(depth + 1).push_back("-");
	}
}



#include "linalg.h"

void MatrixInverse(vector<vector<double> > M, vector<vector<double> > &Minv){
	int n = (int)Minv.size();
	
	alglib::real_2d_array inv;
	inv.setlength(n,n);
	for(int i=0; i<n; i++)
        for(int j=0; j<n; j++)
            inv[i][j] = M[i][j];

	alglib::ae_int_t info;
	alglib::matinvreport rep;

	alglib::rmatrixinverse( inv, info, rep ) ;
	if (info < 0) {
		cout << "Failed to invert" << endl;
		return;
	}

	for(int i=0; i<n; i++)
        for(int j=0; j<n; j++)
            Minv[i][j] = inv[i][j];
}

//Accuracy of such an approach can not exceed the same for double datatype,
//but it is sufficient for our purposes of storing integers (proved in another paper) and
//is stable (will be changed with multiprecision implementation later).
void MatrixInverse(vector<vector<mpf_class> > M, vector<vector<mpf_class> > &Minv){
	int n = (int)Minv.size();

	vector<vector<double>> tmp(n, vector<double>(n));
	vector<vector<double>> tmp_inv(n, vector<double>(n));

	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			tmp[i][j] = M[i][j].get_d();

	MatrixInverse(tmp, tmp_inv);

	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			Minv[i][j] = mpf_class(tmp_inv[i][j]);
}